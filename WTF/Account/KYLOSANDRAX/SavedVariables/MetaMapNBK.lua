
NBK_Options = {
	["Tooltips"] = false,
	["HeadColor"] = {
		[1] = 1,
		[2] = 1,
		[3] = 1,
	},
	["ListColor"] = {
		[1] = 0.95,
		[2] = 0.8,
		[3] = 0.3,
	},
	["TtipColor"] = {
		[1] = 0,
		[2] = 0.75,
		[3] = 0.75,
	},
	["TextColor"] = {
		[1] = 0,
		[2] = 0.75,
		[3] = 0.75,
	},
	["FrameScale"] = 1,
	["PlaySound1"] = false,
	["PlaySound2"] = false,
	["ShowGuild"] = false,
}
NBK_NoteBookData = {
}
