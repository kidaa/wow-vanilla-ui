
BagnonForeverData = {
	["Al'Akir [instant 60] Blizzlike"] = {
		["Kylosandrax"] = {
			[1] = {
				[1] = "16736",
				[2] = "16737",
				[3] = "16730",
				[4] = "16731",
				[5] = "16732",
				[6] = "16733",
				[7] = "16734",
				["s"] = "16,0,4500",
			},
			[2] = {
				["s"] = "16,0,4500",
			},
			[3] = {
				["s"] = "16,0,4500",
			},
			[4] = {
				["s"] = "16,0,4500",
			},
			[0] = {
				[1] = "6948",
				[2] = "117,5",
				[3] = "18042,200",
				[7] = "18381",
				[9] = "12282",
				[11] = "12798",
				[14] = "18537",
				[16] = "16735",
				["s"] = "16,1,",
			},
			[-2] = {
				["s"] = "12,1,",
			},
			["g"] = 0,
		},
	},
	["version"] = "6.6.30",
	["wowVersion"] = "1.12.1",
	["Warsong [12x] Blizzlike"] = {
		["Ryjax"] = {
			[1] = {
				[1] = "19973",
				[16] = "11821:2545:0:0",
				[14] = "17026,2",
				["s"] = "16,0,4500",
				[13] = "17026,20",
				[15] = "19839",
				[12] = "14047,2",
			},
			[2] = {
				[12] = "12587",
				["s"] = "16,0,4500",
				[13] = "21501:904:0:0",
				[14] = "11811",
				[15] = "20658:852:2149:0",
				[16] = "16835",
				[11] = "17026,20",
			},
			[3] = {
				[1] = "19611",
				[2] = "12846",
				[3] = "22108:927:0:0",
				[4] = "20216",
				[5] = "23198",
				[6] = "13965",
				[8] = "16058",
				[9] = "17026,20",
				[10] = "10071:0:604:0",
				[11] = "10228:0:1215:0",
				[12] = "19869",
				[13] = "19058",
				[14] = "19120",
				[15] = "22204:1886:0:0",
				[16] = "19888:849:0:0",
				["s"] = "16,0,4500",
			},
			[4] = {
				[1] = "22718",
				[2] = "18420:1896:0:0",
				[3] = "12793:1892:0:0",
				[4] = "11819",
				[5] = "13118",
				[6] = "12963",
				[7] = "19727",
				[8] = "15290",
				[9] = "943:249:0:0",
				[10] = "22321",
				[11] = "12002:0:947:0",
				[12] = "12544",
				[13] = "15855",
				[14] = "13966",
				[15] = "18522",
				[16] = "22397",
				["s"] = "16,0,4500",
			},
			[5] = {
				[16] = "15063:927:0:0",
				["s"] = "16,0,4500",
				[15] = "22720",
			},
			[6] = {
				[1] = "11925:2545:0:0",
				[2] = "5976",
				[10] = "12219",
				[12] = "12335",
				["s"] = "12,0,10050",
				[9] = "18333",
				[11] = "12337",
				[8] = "18332",
			},
			[7] = {
				[1] = "12840,13",
				[2] = "12841,32",
				[3] = "12843,2",
				[4] = "12753,4",
				[5] = "12035:0:1201:0",
				[7] = "22523,18",
				[8] = "12336",
				[9] = "20558",
				[10] = "22524,4",
				["s"] = "10,0,4245",
			},
			[8] = {
				[19] = "16207",
				["s"] = "24,0,22249",
				[21] = "6339",
				[18] = "11145",
				[22] = "6218",
				[20] = "11130",
			},
			[-1] = {
				[1] = "6559:907:17:0",
				[2] = "17035,2",
				[3] = "8170,4",
				[4] = "6533,3",
				[6] = "13511",
				["s"] = "24,1,",
			},
			[0] = {
				[1] = "6948",
				[2] = "19022",
				[3] = "6532,7",
				[4] = "13457,5",
				[5] = "13444",
				[6] = "3857,2",
				[7] = "4787,2",
				[8] = "8766,20",
				[9] = "8766,10",
				[10] = "20748",
				[11] = "10308,3",
				[12] = "13444,5",
				[14] = "9144,3",
				[15] = "17038,17",
				[16] = "13174,8",
				["s"] = "16,1,",
			},
			[-2] = {
				[1] = "9249",
				[2] = "11078,8",
				[3] = "18250",
				[4] = "7146",
				[5] = "18249",
				["s"] = "12,1,",
			},
			["g"] = 36272879,
		},
		["Zapunzel"] = {
			[1] = {
				[10] = "10940,5",
				[12] = "3827",
				[7] = "20404,5",
				[9] = "10940,20",
				[13] = "17031,9",
				["s"] = "16,0,4500",
			},
			[2] = {
				[10] = "12662,3",
				[12] = "12262",
				["s"] = "16,0,4500",
				[11] = "17056,5",
			},
			[3] = {
				[10] = "18261",
				["s"] = "16,0,4500",
				[9] = "17020,17",
				[8] = "11325,3",
			},
			[4] = {
				[4] = "16685",
				[5] = "16689",
				[6] = "18501",
				[7] = "8592",
				[8] = "18468",
				[15] = "12846",
				["s"] = "16,0,4500",
			},
			[5] = {
				[1] = "14048,10",
				[2] = "14048,10",
				[3] = "14048,10",
				[4] = "16059",
				[5] = "10285,4",
				[6] = "14048,10",
				[7] = "14048,10",
				[8] = "14227,4",
				[9] = "14048,10",
				[10] = "4305,10",
				[11] = "4305,10",
				[12] = "2996,10",
				[13] = "4305",
				[14] = "6339",
				[15] = "6218",
				[16] = "11130",
				["s"] = "16,0,4500",
			},
			[6] = {
				[1] = "2997",
				[2] = "14048,10",
				[3] = "6530,3",
				[4] = "19022",
				[5] = "14048,10",
				[6] = "14048,10",
				[7] = "14048,10",
				[8] = "2321,2",
				[9] = "4339,10",
				[10] = "4337,10",
				[11] = "4339,10",
				[12] = "4337,10",
				[13] = "14048,10",
				[14] = "14048,10",
				[15] = "16204",
				[16] = "14344,2",
				["s"] = "16,0,4500",
			},
			[7] = {
				[1] = "14048,10",
				[2] = "14048,10",
				[3] = "14048,5",
				[4] = "4306,3",
				[5] = "4338,14",
				[6] = "11176,8",
				[7] = "11137,3",
				[8] = "14048,10",
				[9] = "14048,10",
				[10] = "2996,10",
				[11] = "16202,2",
				[12] = "2996,10",
				[13] = "4339,10",
				[14] = "2996,10",
				[15] = "11177,2",
				[16] = "4339,9",
				["s"] = "16,0,11742",
			},
			[-1] = {
				[1] = "14048,10",
				[2] = "14048,10",
				[3] = "14048,10",
				[4] = "14048,10",
				[5] = "3182,5",
				[6] = "14048,10",
				[7] = "14048,10",
				[8] = "14048,10",
				[9] = "14048,10",
				[10] = "14048,10",
				[11] = "14048,10",
				[12] = "14048,10",
				[13] = "14342,3",
				[14] = "14048,10",
				[15] = "14048,10",
				[16] = "14048,10",
				[17] = "14048,10",
				[18] = "14227,10",
				[19] = "14048,10",
				[20] = "14048,10",
				[21] = "14048,10",
				[22] = "4337,4",
				[23] = "14048,10",
				[24] = "14048,10",
				["s"] = "24,1,",
			},
			[0] = {
				[1] = "6948",
				[2] = "12562",
				[3] = "14047,18",
				[4] = "13471",
				[5] = "10285,2",
				[6] = "8079,7",
				[7] = "7910",
				[11] = "18334",
				[12] = "18240,2",
				[13] = "13174,11",
				[14] = "6149,4",
				[15] = "17032,8",
				["s"] = "16,1,",
			},
			["g"] = 26712,
			[-2] = {
				[1] = "11078,7",
				[2] = "18249",
				["s"] = "12,1,",
			},
		},
		["Kylosandrax"] = {
			[1] = {
				[1] = "12735,20",
				[2] = "14344,12",
				[3] = "18335,4",
				[4] = "18334",
				[5] = "13452,5",
				[6] = "13452,2",
				[7] = "3825,3",
				[8] = "8411",
				[9] = "5829,3",
				[10] = "10307,4",
				[11] = "18269,4",
				[12] = "8952,2",
				[13] = "5665",
				[14] = "14530,3",
				[15] = "8951,2",
				[16] = "5119,4",
				["s"] = "16,0,4500",
			},
			[2] = {
				[1] = "5120,4",
				[2] = "5471,4",
				[3] = "5566,4",
				[4] = "11417,3",
				[5] = "14047,14",
				[6] = "11418,2",
				[7] = "5117",
				[8] = "8146",
				[10] = "10310",
				[11] = "6256",
				["s"] = "16,0,4500",
			},
			[3] = {
				[12] = "22331",
				["s"] = "16,0,4500",
				[13] = "11669",
				[14] = "18370",
				[15] = "13209",
				[16] = "21456",
				[9] = "11284,197",
				[10] = "11284,199",
				[11] = "13380",
			},
			[4] = {
				[1] = "12952:2545:1366:0",
				[2] = "19871",
				[3] = "21805",
				[4] = "14623:2545:0:0",
				[5] = "21503",
				[6] = "11810",
				[7] = "11285,184",
				[8] = "19896:1900:0:0",
				[9] = "12846",
				[10] = "19862:929:0:0",
				[11] = "13966",
				[12] = "20130",
				[13] = "18503:1892:0:0",
				[14] = "19913:929:0:0",
				[15] = "21996:1886:0:0",
				[16] = "21479:856:0:0",
				["s"] = "16,0,4500",
			},
			[5] = {
				[1] = "22202",
				[2] = "3858,2",
				[3] = "3859,5",
				[4] = "2841,4",
				[5] = "13217",
				[6] = "8703",
				[7] = "19120",
				[8] = "10418",
				[9] = "14624",
				[10] = "21994",
				[11] = "13072:856:0:0",
				[13] = "14621:929:0:0",
				[14] = "11734",
				[15] = "18332",
				[16] = "12359,4",
				["s"] = "16,0,4500",
			},
			[6] = {
				[1] = "3576,6",
				[2] = "3576,20",
				[10] = "22321",
				[11] = "19694:2545:0:0",
				[13] = "19695",
				[14] = "22617",
				[15] = "22607",
				[16] = "2842,11",
				["s"] = "16,0,4500",
			},
			[7] = {
				[1] = "19693:1892:0:0",
				[2] = "19910:1900:0:0",
				[3] = "19948",
				[4] = "13527",
				[7] = "20404,5",
				[8] = "20809",
				[9] = "23024",
				[10] = "20422",
				[11] = "12843,2",
				[12] = "22593",
				[13] = "7912,20",
				[14] = "7912,5",
				[15] = "2901",
				[16] = "18365",
				["s"] = "16,0,4500",
			},
			[8] = {
				[1] = "22484,2",
				[2] = "20801,3",
				[6] = "20944",
				[7] = "21166",
				[8] = "21167",
				[9] = "22524,10",
				[10] = "22523,20",
				[11] = "12841,2",
				[12] = "12840,48",
				[13] = "12753,3",
				[14] = "18240,2",
				[16] = "11176",
				["s"] = "16,0,4500",
			},
			[9] = {
				[2] = "11176,20",
				[7] = "16059",
				[8] = "20873",
				[9] = "20886",
				[10] = "20886",
				[11] = "19022",
				[12] = "11083,16",
				[13] = "6365",
				[14] = "11176,20",
				["s"] = "14,0,3914",
			},
			[10] = {
				[1] = "16202,5",
				[2] = "11082,3",
				[3] = "11175,4",
				[4] = "16204,20",
				[5] = "16207",
				[6] = "10939,3",
				[7] = "11145",
				[8] = "14343,4",
				[9] = "11138,3",
				[10] = "11130",
				[11] = "16203,3",
				[12] = "16204,20",
				[13] = "11137,20",
				[14] = "11135,2",
				[15] = "11139,2",
				[16] = "11176,20",
				[17] = "6218",
				[18] = "16203,10",
				[19] = "16204,15",
				[20] = "6339",
				[22] = "11135,10",
				[23] = "11137,9",
				[24] = "11083,20",
				["s"] = "24,0,22249",
			},
			[-1] = {
				[1] = "19287",
				[2] = "19575",
				[3] = "16733",
				[4] = "19869:856:0:0",
				[5] = "12602:929:0:0",
				[6] = "3577,18",
				[7] = "2838,20",
				[8] = "3860,3",
				[9] = "7911,3",
				[10] = "12365,10",
				[11] = "12365,20",
				[12] = "7912,20",
				[13] = "6037,2",
				[14] = "6037,20",
				[15] = "8314:906:0:0",
				[16] = "18349:856:0:0",
				[17] = "16732",
				[18] = "19888:1889:0:0",
				[19] = "19824",
				[20] = "13959",
				[21] = "20216",
				[22] = "11812",
				[23] = "2836,7",
				[24] = "7912,20",
				["s"] = "24,1,",
			},
			[0] = {
				[1] = "6948",
				[2] = "12404,9",
				[3] = "5759",
				[4] = "5759",
				[5] = "5759",
				[6] = "5759",
				[7] = "5759",
				[8] = "5759",
				[9] = "5759",
				[10] = "16204,3",
				[11] = "4589,4",
				[12] = "4590,4",
				[13] = "20394",
				[14] = "13180,13",
				[15] = "5956",
				[16] = "12643,6",
				["s"] = "16,1,",
			},
			[-2] = {
				[1] = "11078,9",
				[2] = "9249",
				[3] = "11000",
				[4] = "18249",
				[5] = "12382",
				[6] = "21761",
				["s"] = "12,1,",
			},
			["g"] = 12639,
		},
		["Ryvok"] = {
			[1] = {
				[1] = "5635,2",
				[4] = "16084",
				[6] = "8766,4",
				[7] = "2677,2",
				[8] = "1015,2",
				[10] = "12202,10",
				[11] = "6149,3",
				[13] = "17033,5",
				[15] = "12205,6",
				["s"] = "16,0,4500",
			},
			[2] = {
				[5] = "16247",
				["s"] = "16,0,4500",
				[9] = "6149,5",
				[11] = "9149",
				[10] = "4500",
			},
			[3] = {
				[12] = "4821",
				["s"] = "16,0,4500",
				[13] = "16671:66:0:0",
				[7] = "6149,5",
				[8] = "21177,60",
				[9] = "22234",
				[10] = "18386",
				[11] = "2815",
			},
			[4] = {
				[1] = "22319",
				[2] = "22528,4",
				[3] = "23024",
				[5] = "5758",
				[6] = "21177,100",
				[7] = "19123",
				[8] = "22720",
				[9] = "11816",
				[10] = "22275",
				[11] = "21500",
				[12] = "22713",
				["s"] = "16,0,4500",
			},
			[5] = {
				[1] = "18046",
				[4] = "2251,10",
				[5] = "12202,10",
				[6] = "3667,10",
				[8] = "1081,10",
				[9] = "2251,3",
				[11] = "1015,10",
				[12] = "3685,6",
				[15] = "12205,10",
				[16] = "12662,2",
				["s"] = "16,0,4500",
			},
			[6] = {
				[1] = "729,3",
				[2] = "19441",
				[3] = "2414",
				[4] = "12205,10",
				[5] = "3667,10",
				[6] = "12205,10",
				[9] = "12205,10",
				[10] = "1081,6",
				[11] = "8151,4",
				[12] = "11202",
				[15] = "1015,10",
				["s"] = "16,0,4500",
			},
			[7] = {
				[2] = "11040,2",
				[16] = "1080,2",
				["s"] = "16,0,4500",
			},
			[8] = {
				[1] = "7974,10",
				[4] = "12202,4",
				["s"] = "16,0,4500",
			},
			[-1] = {
				[1] = "3667,3",
				[2] = "12208,4",
				[3] = "4500",
				[4] = "12841,4",
				[5] = "12184,10",
				[6] = "12202,10",
				[7] = "12037,4",
				[8] = "12037,10",
				[9] = "12203,5",
				[10] = "3685,10",
				[11] = "12840,8",
				[12] = "3928,5",
				[13] = "12843,3",
				[14] = "3928,5",
				[15] = "12735,5",
				[16] = "7974,10",
				[17] = "4500",
				[18] = "7974,8",
				[19] = "7909",
				[20] = "12205,4",
				[21] = "7910",
				[23] = "12202,10",
				[24] = "1081,10",
				["s"] = "24,1,",
			},
			[0] = {
				[1] = "6948",
				[2] = "13180,13",
				[3] = "13174,20",
				[4] = "10285,8",
				[5] = "3577",
				[6] = "21920",
				[7] = "21803",
				[8] = "16251",
				[9] = "13462,5",
				[10] = "13462",
				[12] = "8766,20",
				[13] = "8766,20",
				[15] = "6218",
				[16] = "6454",
				["s"] = "16,1,",
			},
			["g"] = 256887,
			[-2] = {
				[1] = "5396",
				[2] = "11078",
				["s"] = "12,1,",
			},
		},
		["Banjax"] = {
			[1] = {
				[6] = "4500",
				["s"] = "12,0,1725",
				[11] = "8952,20",
				[12] = "8952,11",
			},
			[2] = {
				[1] = "15846",
				["s"] = "14,0,1685",
				[13] = "8953,2",
				[14] = "6149,3",
			},
			[3] = {
				[1] = "5665",
				["s"] = "16,0,4500",
				[13] = "8952,20",
				[15] = "8150,8",
				[14] = "13180,2",
			},
			[4] = {
				[12] = "11285,200",
				["s"] = "14,1653,7371",
				[13] = "11285,200",
				[14] = "11285,200",
				[7] = "11285,200",
				[8] = "11285,200",
				[9] = "11285,200",
				[10] = "11285,200",
				[11] = "11285,200",
				[6] = "11285,53",
			},
			[5] = {
				[1] = "4234,10",
				[2] = "4231,6",
				[3] = "8150,20",
				[5] = "8368",
				[6] = "8170,20",
				[7] = "18333",
				[8] = "18332",
				["s"] = "10,0,5575",
			},
			[6] = {
				[1] = "2319,11",
				[2] = "2318,20",
				[3] = "2318,20",
				[9] = "15749",
				["s"] = "10,0,804",
			},
			[7] = {
				[1] = "11284,200",
				[2] = "11284,200",
				[3] = "11284,87",
				["s"] = "14,0,7372",
			},
			[-1] = {
				[1] = "8170,6",
				[2] = "11814",
				[3] = "11765:0:525:0",
				[4] = "11812",
				[5] = "8167,7",
				[6] = "15422,5",
				[7] = "4233,8",
				[8] = "11188",
				[9] = "20560",
				[10] = "8172,20",
				[11] = "8169,10",
				[12] = "2318,7",
				[13] = "4304,20",
				[14] = "4304,7",
				[15] = "8154,5",
				[16] = "8172,4",
				[17] = "4236,8",
				[18] = "22593",
				[19] = "5116,3",
				[20] = "8146,3",
				[21] = "8169,2",
				[22] = "12735,6",
				[23] = "15416,2",
				["s"] = "24,1,",
			},
			[0] = {
				[1] = "6948",
				[3] = "15409,8",
				[6] = "18269,10",
				[10] = "12202,5",
				[11] = "12202,10",
				[12] = "8766,20",
				[13] = "8766,20",
				[14] = "8766,20",
				[15] = "7005",
				[16] = "13174,20",
				["s"] = "16,1,",
			},
			[-2] = {
				[1] = "9249",
				[2] = "9275",
				[3] = "7146",
				[4] = "11078,6",
				[5] = "12382",
				["s"] = "12,1,",
			},
			["g"] = 368898,
		},
		["Stormslinger"] = {
			[1] = {
				[1] = "17030,5",
				[2] = "3369,8",
				[3] = "3356,13",
				[4] = "3575,15",
				[5] = "2453,20",
				[6] = "13464,20",
				[7] = "18256",
				[9] = "8846,8",
				[10] = "13466,2",
				[11] = "3357",
				[12] = "10286,8",
				[13] = "5177",
				[14] = "12562",
				["s"] = "14,0,14046",
			},
			[2] = {
				[1] = "3358,20",
				[2] = "4625,20",
				[3] = "8836,20",
				[4] = "3358,20",
				[5] = "13468,14",
				[6] = "2453,20",
				[7] = "2453,5",
				[8] = "13467,6",
				[9] = "13464",
				[10] = "4625,20",
				[11] = "4625,17",
				[12] = "3358,5",
				[13] = "5176",
				[14] = "12846",
				["s"] = "14,0,14046",
			},
			[3] = {
				[1] = "3820,6",
				[2] = "3818,14",
				[3] = "8836,10",
				[5] = "8153,12",
				[13] = "5175",
				[14] = "20656:0:2151:0",
				[15] = "16680",
				[16] = "12025:0:861:0",
				["s"] = "16,0,4500",
			},
			[4] = {
				[1] = "20656:0:2153:0",
				[2] = "18524",
				[3] = "22204",
				[4] = "11926",
				[5] = "22234",
				[6] = "11925",
				[7] = "20659:0:2153:0",
				[8] = "18522",
				[9] = "11815",
				[10] = "13358",
				[11] = "5178",
				[12] = "22404",
				[13] = "18485",
				[14] = "11858",
				[15] = "6414",
				[16] = "11814",
				["s"] = "16,0,4500",
			},
			[5] = {
				["s"] = "12,0,22250",
			},
			[6] = {
				["s"] = "14,0,14046",
				[6] = "6358",
			},
			[7] = {
				[9] = "8150",
				["s"] = "12,0,10050",
			},
			[8] = {
				[10] = "20558",
				["s"] = "10,0,4497",
				[9] = "22047",
				[8] = "22713",
			},
			[-1] = {
				[1] = "12753,3",
				[19] = "12735",
				[16] = "11020",
				["s"] = "24,1,",
				[18] = "9030",
				[7] = "9262,3",
			},
			[0] = {
				[1] = "5665",
				[2] = "18746",
				[3] = "3821,4",
				[4] = "8950",
				[5] = "13490",
				[6] = "8925",
				[7] = "13464,20",
				[8] = "13465",
				[10] = "14047,6",
				[12] = "3372,12",
				[13] = "3372,20",
				[14] = "13180,3",
				[15] = "9149",
				[16] = "17030",
				["s"] = "16,1,",
			},
			["g"] = 94749,
			[-2] = {
				[1] = "5020",
				[2] = "7146",
				[3] = "11078",
				["s"] = "12,1,",
			},
		},
		["Kashonar"] = {
			[1] = {
				["s"] = "16,0,4500",
			},
			[2] = {
				["s"] = "16,0,4500",
			},
			[3] = {
				["s"] = "16,0,4500",
			},
			[-1] = {
				["s"] = "24,199,",
			},
			[0] = {
				[1] = "6948",
				[2] = "2581",
				[3] = "2455",
				[4] = "1251,4",
				[5] = "2589",
				[15] = "5060",
				["s"] = "16,1,",
			},
			["g"] = 92120,
			[-2] = {
				["s"] = "4,1,",
			},
		},
		["Sixofnine"] = {
			[1] = {
				[1] = "3174,8",
				[2] = "10369",
				[3] = "7077",
				[4] = "14499",
				[5] = "12803,10",
				[6] = "7080",
				[7] = "13462,2",
				[8] = "18334",
				[9] = "10592,5",
				[10] = "12645",
				[11] = "13493",
				[12] = "814,10",
				[13] = "6338",
				[14] = "12695",
				["s"] = "14,0,14046",
			},
			[2] = {
				[1] = "19698,15",
				[2] = "8956,2",
				[3] = "12804,3",
				[4] = "8146,5",
				[5] = "17056,10",
				[6] = "19703,7",
				[7] = "6338",
				[8] = "9224",
				[9] = "14047,19",
				[10] = "7912,4",
				[11] = "1529,20",
				[12] = "7909,20",
				[13] = "18333",
				[14] = "10285,5",
				[15] = "19715,3",
				[16] = "12364,4",
				["s"] = "16,0,4500",
			},
			[3] = {
				[1] = "13926,2",
				[2] = "4425,2",
				[3] = "20860",
				[4] = "19704,11",
				[5] = "10592,5",
				[6] = "11732",
				[7] = "11734",
				[8] = "19710,3",
				[9] = "3182,3",
				[10] = "7972,10",
				[11] = "19701,6",
				[12] = "18332",
				[13] = "1705,20",
				[14] = "18769",
				[15] = "12808",
				[16] = "19702,7",
				["s"] = "16,0,4500",
			},
			[4] = {
				[1] = "19700,12",
				[2] = "19699,7",
				[3] = "16716",
				[4] = "3174,10",
				[5] = "19706,19",
				[6] = "3174,10",
				[7] = "19705,9",
				[8] = "19726,4",
				["s"] = "16,0,4500",
			},
			[5] = {
				[1] = "3859,20",
				[2] = "3859,20",
				[3] = "3864,20",
				[4] = "7909,20",
				[5] = "7909,20",
				[6] = "7910,20",
				[7] = "7910,20",
				[8] = "3577,20",
				[9] = "3577,20",
				[10] = "3577,20",
				[11] = "18945,100",
				[12] = "3859,20",
				[13] = "2842,20",
				[14] = "2842,20",
				[15] = "3577,20",
				[16] = "19933,8",
				["s"] = "16,0,4500",
			},
			[6] = {
				[1] = "7972,10",
				[2] = "3864,8",
				[3] = "1206,6",
				[4] = "22529,26",
				[5] = "7972,10",
				[6] = "7972,10",
				[7] = "10593,12",
				[8] = "5498,3",
				[9] = "5500,10",
				[10] = "11370,2",
				[11] = "7077,6",
				[12] = "7972,10",
				[13] = "20866",
				[14] = "22527,6",
				[15] = "7972,10",
				[16] = "9260,10",
				["s"] = "16,0,4500",
			},
			[7] = {
				[1] = "12808,7",
				[2] = "18945,100",
				[3] = "7909,20",
				[4] = "3577,20",
				[5] = "7911,2",
				[6] = "5498,20",
				[7] = "11382,2",
				[8] = "7076",
				[9] = "7068,2",
				[10] = "8146,5",
				[11] = "17010",
				[12] = "11754,20",
				[13] = "20862,6",
				[14] = "12803,8",
				[15] = "8151,5",
				[16] = "12360,3",
				["s"] = "16,0,4500",
			},
			[8] = {
				[1] = "8153,2",
				[2] = "7972,10",
				[3] = "11754,12",
				[4] = "12804,2",
				[5] = "20861,5",
				[6] = "20860,9",
				[7] = "8956,5",
				[8] = "8956,4",
				[9] = "9036,5",
				[10] = "9264",
				[11] = "9197,2",
				[12] = "7078,10",
				[13] = "7972,10",
				[14] = "3829,2",
				[15] = "8152,15",
				[16] = "5117,2",
				["s"] = "16,0,4500",
			},
			[9] = {
				[1] = "6370,6",
				[2] = "18945,100",
				[3] = "12808,10",
				[4] = "7972,10",
				[5] = "5117,10",
				[6] = "22525,10",
				[7] = "5635,5",
				[8] = "5635,4",
				[9] = "7971,14",
				[10] = "20865,10",
				[11] = "18945,3",
				[12] = "20864,9",
				[13] = "8150,20",
				[14] = "12800,4",
				[15] = "7078,10",
				[16] = "4623,2",
				["s"] = "16,0,4500",
			},
			[10] = {
				[1] = "18944,57",
				[2] = "7067,5",
				[3] = "22526,10",
				[8] = "12808,10",
				[9] = "8165,5",
				[10] = "20520,5",
				[11] = "1210,2",
				[12] = "9262",
				[13] = "15420,20",
				[14] = "8150,2",
				["s"] = "16,0,4500",
			},
			[-1] = {
				[1] = "20513",
				[2] = "7078,4",
				[3] = "22528,20",
				[4] = "7078,10",
				[5] = "7972,10",
				[6] = "19726,12",
				[7] = "1529,15",
				[8] = "7078,10",
				[9] = "1206,20",
				[10] = "18945,100",
				[11] = "7910,20",
				[12] = "20858,7",
				[13] = "8146,5",
				[14] = "18945,100",
				[15] = "12808,10",
				[16] = "18945,100",
				[17] = "3864,20",
				[18] = "12607,9",
				[19] = "1705,15",
				[20] = "7972,10",
				[21] = "7082",
				[22] = "7075,3",
				[23] = "7909,3",
				[24] = "11754,20",
				["s"] = "24,1,",
			},
			[0] = {
				[1] = "19259",
				[2] = "19261",
				[3] = "5116,3",
				[4] = "19709",
				[5] = "2728",
				[6] = "2772",
				[7] = "8956,5",
				[8] = "9260,9",
				[9] = "6533",
				[10] = "10286,3",
				[11] = "4589",
				[12] = "8956,5",
				[13] = "10286,2",
				[14] = "7910,2",
				[15] = "8392",
				[16] = "9206",
				["s"] = "16,1,",
			},
			[-2] = {
				["s"] = "4,1,",
			},
			["g"] = 316558,
		},
	},
	["Emerald Dream [1x] Blizzlike"] = {
		["Nameplate"] = {
			[0] = {
				[1] = "117,5",
				[2] = "6948",
				[3] = "25020",
				["s"] = "16,1,",
			},
			["g"] = 0,
			[-2] = {
				["s"] = "4,1,",
			},
		},
		["Ryvok"] = {
			[0] = {
				[1] = "6948",
				[2] = "159,5",
				[3] = "2070,5",
				[4] = "25020",
				["s"] = "16,1,",
			},
			[-2] = {
				["s"] = "4,1,",
			},
			["g"] = 0,
		},
	},
}
