
TheoryCraft_SetBonuses = {
	["Beaststalker Armor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "+40 Attack Power.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Your normal ranged attacks have a 4% chance of restoring 200 mana.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["Zanzil's Concentration"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Improves your chance to hit with spells by 1%.",
		},
		[2] = {
			["pieces"] = 2,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 6.",
		},
	},
	["Primal Blessing"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Grants a small chance when ranged or melee damage is dealt to infuse the wielder with a blessing from the Primal Gods. Ranged and melee attack power increased by 300 for 12 seconds.",
		},
	},
	["Lightforge Armor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "+40 Attack Power.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Chance on melee attack to increase your damage and healing done by magical spells and effects by up to 95 for 10 sec.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["Embrace of the Viper"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Increases damage done by Nature spells and effects by up to 7.",
		},
		[2] = {
			["pieces"] = 3,
			["text"] = "Increased Staves +2.",
		},
		[3] = {
			["pieces"] = 4,
			["text"] = "Increases healing done by spells and effects by up to 11.",
		},
		[4] = {
			["pieces"] = 5,
			["text"] = "+10 Intellect.",
		},
	},
	["Feralheart Raiment"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+8 All Resistances.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "When struck in combat has a chance of returning 300 mana, 10 rage, or 40 energy to the wearer. ",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 15.",
		},
		[4] = {
			["pieces"] = 6,
			["text"] = "+26 Attack Power.",
		},
		[5] = {
			["pieces"] = 8,
			["text"] = "+200 Armor.",
		},
	},
	["Battlegear of Heroism"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+8 All Resistances.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "Chance on melee attack to heal you for 88 to 132.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "+40 Attack Power.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+200 Armor.",
		},
	},
	["Vindicator's Battlegear"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Increases your chance to block attacks with a shield by 2%.",
		},
		[2] = {
			["pieces"] = 3,
			["text"] = "Decreases the cooldown of Intimidating Shout by 15 sec.",
		},
		[3] = {
			["pieces"] = 5,
			["text"] = "Decrease the rage cost of Whirlwind by 3.",
		},
	},
	["Dreadmist Raiment"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 23.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "When struck in combat has a chance of causing the attacker to flee in terror for 2 seconds. ",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["Battlegear of Valor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "+40 Attack Power.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Chance on melee attack to heal you for 88 to 132.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["Bloodvine Garb"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "Improves your chance to get a critical strike with spells by 2%.",
		},
	},
	["Deathbone Guardian"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Increased Defense +3.",
		},
		[2] = {
			["pieces"] = 3,
			["text"] = "+50 Armor.",
		},
		[3] = {
			["pieces"] = 4,
			["text"] = "+15 All Resistances.",
		},
		[4] = {
			["pieces"] = 5,
			["text"] = "Increases your chance to parry an attack by 1%.",
		},
	},
	["Magister's Regalia"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 23.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "When struck in combat has a chance of freezing the attacker in place for 3 sec.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["Dal'Rend's Arms"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+50 Attack Power.",
		},
	},
	["Stormshroud Armor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "5% chance of dealing 15 to 25 Nature damage on a successful melee attack.",
		},
		[2] = {
			["pieces"] = 3,
			["text"] = "2% chance on melee attack of restoring 30 energy.",
		},
		[3] = {
			["pieces"] = 4,
			["text"] = "+14 Attack Power.",
		},
	},
	["Wildheart Raiment"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "+26 Attack Power.",
		},
		[3] = {
			["pieces"] = 4,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 15.",
		},
		[4] = {
			["pieces"] = 6,
			["text"] = "When struck in combat has a chance of returning 300 mana, 10 rage, or 40 energy to the wearer. ",
		},
		[5] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["The Five Thunders"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+8 All Resistances.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "Chance on spell cast to increase your damage and healing by up to 95 for 10 sec.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 23.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+200 Armor.",
		},
	},
	["Prayer of the Primal"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Increases healing done by spells and effects by up to 33.",
		},
	},
	["The Elements"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 23.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Chance on spell cast to increase your damage and healing by up to 95 for 10 sec.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["Battlegear of Unyielding Strength"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "-2 rage cost to Intercept.",
		},
	},
	["Devilsaur Armor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Improves your chance to hit by 2%.",
		},
	},
	["Cenarion Raiment"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "Damage dealt by Thorns increased by 4 and duration increased by 50%.",
		},
		[2] = {
			["pieces"] = 5,
			["text"] = "Improves your chance to get a critical strike with spells by 2%.",
		},
		[3] = {
			["pieces"] = 8,
			["text"] = "Reduces the cooldown of your Tranquility and Hurricane spells by 50%.",
		},
	},
	["Chain of the Scarlet Crusade"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+10 Armor.",
		},
		[2] = {
			["pieces"] = 3,
			["text"] = "Increased Defense +1.",
		},
		[3] = {
			["pieces"] = 4,
			["text"] = "+5 Shadow Resistance.",
		},
		[4] = {
			["pieces"] = 5,
			["text"] = "+15 Attack Power when fighting Undead.",
		},
		[5] = {
			["pieces"] = 6,
			["text"] = "Improves your chance to hit by 1%.",
		},
	},
	["Twilight Trappings"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "Bestows the wearer with the evil aura of a Twilight's Hammer cultist.",
		},
	},
	["Major Mojo Infusion"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+30 Attack Power.",
		},
	},
	["Ironfeather Armor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 20.",
		},
	},
	["Vestments of the Devout"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "Increases damage and healing done by magical spells and effects by up to 23.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "When struck in combat has a chance of shielding the wearer in a protective shield which will absorb 350 damage.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
	["The Earthfury"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "The radius of your totems that affect friendly targets is increased to 30 yd.",
		},
		[2] = {
			["pieces"] = 5,
			["text"] = "After casting your Healing Wave or Lesser Healing Wave spell, gives you a 25% chance to gain Mana equal to 35% of the base cost of the spell.",
		},
		[3] = {
			["pieces"] = 8,
			["text"] = "Your Healing Wave will now jump to additional nearby targets. Each jump reduces the effectiveness of the heal by 80%, and the spell will jump to up to two additional targets.",
		},
	},
	["Volcanic Armor"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "5% chance of dealing 15 to 25 Fire damage on a successful melee attack.",
		},
	},
	["The Darksoul"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "Increased Defense +20.",
		},
	},
	["Imperial Plate"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+100 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "+28 Attack Power.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "+18 Stamina.",
		},
	},
	["Stormrage Raiment"] = {
		[1] = {
			["pieces"] = 3,
			["text"] = "Allows 15% of your Mana regeneration to continue while casting.",
		},
		[2] = {
			["pieces"] = 5,
			["text"] = "Reduces the casting time of your Regrowth spell by 0.2 sec.",
		},
		[3] = {
			["pieces"] = 8,
			["text"] = "Increases the duration of your Rejuvenation spell by 3 sec.",
		},
	},
	["Haruspex's Garb"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "Restores 4 mana per 5 sec.",
		},
		[2] = {
			["pieces"] = 3,
			["text"] = "Increases the duration of Faerie Fire by 5 sec.",
		},
		[3] = {
			["pieces"] = 5,
			["text"] = "Increases the critical hit chance of your Starfire spell 3%.",
		},
	},
	["Shadowcraft Armor"] = {
		[1] = {
			["pieces"] = 2,
			["text"] = "+200 Armor.",
		},
		[2] = {
			["pieces"] = 4,
			["text"] = "+40 Attack Power.",
		},
		[3] = {
			["pieces"] = 6,
			["text"] = "Chance on melee attack to restore 35 energy.",
		},
		[4] = {
			["pieces"] = 8,
			["text"] = "+8 All Resistances.",
		},
	},
}
TheoryCraft_MitigationMobs = {
	["Spire Scarab"] = {
		[1] = 518,
	},
	["Earthborer"] = {
		[1] = 411,
	},
	["Bristleback Thornweaver"] = {
		[1] = 1298,
	},
	["Desert Rumbler"] = {
		[1] = 3359,
	},
	["Dark Iron Slaver"] = {
		[1] = -2168,
	},
	["Skeletal Guardian"] = {
		[1] = 2797,
	},
	["Vilebranch Wolf Pup"] = {
		[1] = 2750,
	},
	["Anvilrage Captain"] = {
		[1] = 3483,
	},
	["Gordok Hyena"] = {
		[1] = 5020,
	},
	["Jammal'an the Prophet"] = {
		[1] = -764,
	},
	["Voidwalker Minion"] = {
		[1] = 2184,
	},
	["Venomtip Scorpid"] = {
		[1] = 3378,
	},
	["Bloodhound"] = {
		[1] = 2894,
	},
	["Savannah Patriarch"] = {
		[1] = 1266,
	},
	["General Drakkisath"] = {
		[1] = 5423,
	},
	["Shrike Bat"] = {
		[1] = 1627,
	},
	["Longtooth Runner"] = {
		[1] = 2420,
	},
	["Whip Lasher"] = {
		[1] = 3083,
	},
	["Haldarr Satyr"] = {
		[1] = 2871,
	},
	["Searing Blade Cultist"] = {
		[1] = 799,
	},
	["Kresh"] = {
		[1] = 1308,
	},
	["Clutchmother Zavas"] = {
		[1] = 3035,
	},
	["Qiraji Swarmguard"] = {
		[1] = -286,
	},
	["Scarshield Quartermaster"] = {
		[1] = -1043,
	},
	["Hydrospawn"] = {
		[1] = -1997,
	},
	["Treasure Hunting Buccaneer"] = {
		[1] = 2900,
	},
	["Cavalier Durgen"] = {
		[1] = 3471,
	},
	["Hate'rel"] = {
		[1] = 2177,
	},
	["Gurubashi Blood Drinker"] = {
		[1] = 3630,
	},
	["Wailing Banshee"] = {
		[1] = 3281,
	},
	["Haldarr Trickster"] = {
		[1] = 2687,
	},
	["Dustbelcher Lord"] = {
		[1] = -509,
	},
	["Shadowforge Peasant"] = {
		[1] = 2639,
	},
	["Rage Talon Flamescale"] = {
		[1] = 2608,
	},
	["Lethtendris"] = {
		[1] = -1750,
	},
	["Murk Slitherer"] = {
		[1] = 2913,
	},
	["Anvilrage Overseer"] = {
		[1] = 2262,
	},
	["Spire Spider"] = {
		[1] = -34,
	},
	["Gordok Enforcer"] = {
		[1] = 3474,
	},
	["Drywhisker Kobold"] = {
		[1] = 2509,
	},
	["Awbee"] = {
		[1] = 370,
	},
	["Razormane Thornweaver"] = {
		[1] = 652,
	},
	["Witherbark Witch Doctor"] = {
		[1] = 2138,
	},
	["Venom Mist Lurker"] = {
		[1] = 2996,
	},
	["Mad Voidwalker"] = {
		[1] = 3712,
	},
	["Snake"] = {
		[1] = 283,
	},
	["Anvilrage Reservist"] = {
		[1] = 3419,
	},
	["Kolkar Stormer"] = {
		[1] = 816,
	},
	["Archivist Galford"] = {
		[1] = 2852,
	},
	["Starving Blisterpaw"] = {
		[1] = 2548,
	},
	["Atal'ai Witch Doctor"] = {
		[1] = -535,
	},
	["Gorishi Wasp"] = {
		[1] = -847,
	},
	["Gordok Mage-Lord"] = {
		[1] = 1692,
	},
	["Kolkar Bloodcharger"] = {
		[1] = 1142,
	},
	["Drywallow Snapper"] = {
		[1] = 2498,
	},
	["Lorekeeper Polkelt"] = {
		[1] = -564,
	},
	["Baron Geddon"] = {
		[1] = 4598,
	},
	["Gnarl Leafbrother"] = {
		[1] = 2484,
	},
	["Mana Remnant"] = {
		[1] = 2682,
	},
	["Crimson Sorcerer"] = {
		[1] = 14,
	},
	["Spirestone Warlord"] = {
		[1] = -416,
	},
	["Shadowforge Citizen"] = {
		[1] = 2326,
	},
	["Shazzrah"] = {
		[1] = -928,
	},
	["Ironfur Bear"] = {
		[1] = 2481,
	},
	["Wastewander Thief"] = {
		[1] = 2719,
	},
	["Thundering Boulderkin"] = {
		[1] = 1937,
	},
	["Residual Monstrosity"] = {
		[1] = 3508,
	},
	["Hive'Ashi Defender"] = {
		[1] = 3535,
	},
	["Mosshoof Runner"] = {
		[1] = 2722,
	},
	["Hakkar"] = {
		[1] = 3946,
	},
	["Fungal Ooze"] = {
		[1] = 2696,
	},
	["Netherwalker"] = {
		[1] = 1125,
	},
	["Guard Slip'kik"] = {
		[1] = -1292,
	},
	["Stromgarde Defender"] = {
		[1] = 3048,
	},
	["Houndmaster Loksey"] = {
		[1] = 2178,
	},
	["Mottled Boar"] = {
		[1] = 170,
	},
	["Gurubashi Headhunter"] = {
		[1] = 2932,
	},
	["Gurubashi Berserker"] = {
		[1] = 3587,
	},
	["Raging Owlbeast"] = {
		[1] = 3549,
	},
	["Antu'sul"] = {
		[1] = 3468,
	},
	["Captain Qeez"] = {
		[1] = 2757,
	},
	["Slave Worker"] = {
		[1] = 2900,
	},
	["Eviscerator"] = {
		[1] = -1723,
	},
	["Spiteful Phantom"] = {
		[1] = 520,
	},
	["Twilight Keeper Mayna"] = {
		[1] = 3593,
	},
	["Bijou"] = {
		[1] = -287,
	},
	["Doomforge Arcanasmith"] = {
		[1] = 1890,
	},
	["Syndicate Highwayman"] = {
		[1] = 2076,
	},
	["Highland Fleshstalker"] = {
		[1] = 2516,
	},
	["Thunderhead Hippogryph"] = {
		[1] = 2772,
	},
	["Phase Lasher"] = {
		[1] = -2318,
	},
	["Grimtotem Raider"] = {
		[1] = -782,
	},
	["Deathclasp"] = {
		[1] = 3307,
	},
	["Atal'ai Warrior"] = {
		[1] = 2911,
	},
	["Scarlet Monk"] = {
		[1] = 2253,
	},
	["Grimtotem Geomancer"] = {
		[1] = 1624,
	},
	["Firelord"] = {
		[1] = 1681,
	},
	["King Mosh"] = {
		[1] = 4239,
	},
	["Scorpid Tail Lasher"] = {
		[1] = 2794,
	},
	["Shadowsworn Adept"] = {
		[1] = 3823,
	},
	["Son of Hakkar"] = {
		[1] = 3155,
	},
	["Mangled Cadaver"] = {
		[1] = -254,
	},
	["Wastewander Assassin"] = {
		[1] = 2738,
	},
	["Twilight Fire Guard"] = {
		[1] = 2882,
	},
	["Smolderthorn Shadow Hunter"] = {
		[1] = 789,
	},
	["Scarlet Hunter"] = {
		[1] = 3926,
	},
	["Molten Elemental"] = {
		[1] = 799,
	},
	["Broken Cadaver"] = {
		[1] = 5534,
	},
	["Zhevra Courser"] = {
		[1] = 1234,
	},
	["Wayward Buzzard"] = {
		[1] = 2445,
	},
	["Druid of the Fang"] = {
		[1] = 1147,
	},
	["King Gordok"] = {
		[1] = 4181,
	},
	["Hedrum the Creeper"] = {
		[1] = 3162,
	},
	["Windfury Matriarch"] = {
		[1] = 678,
	},
	["Ice Thistle Patriarch"] = {
		[1] = 3775,
	},
	["Bile Slime"] = {
		[1] = 3825,
	},
	["Arcane Aberration"] = {
		[1] = 2910,
	},
	["Razzashi Cobra"] = {
		[1] = 3571,
	},
	["Scorpok Stinger"] = {
		[1] = 2818,
	},
	["Ok'thor the Breaker"] = {
		[1] = 2585,
	},
	["Smolderthorn Mystic"] = {
		[1] = 27,
	},
	["Deathlash Scorpid"] = {
		[1] = 3692,
	},
	["Vilebranch Aman'zasi Guard"] = {
		[1] = 3015,
	},
	["Slimeshell Makrura"] = {
		[1] = 1379,
	},
	["Inferno Elemental"] = {
		[1] = 3113,
	},
	["Thunderhawk Hatchling"] = {
		[1] = 1517,
	},
	["Southsea Pirate"] = {
		[1] = 2838,
	},
	["Weaver"] = {
		[1] = -701,
	},
	["Blackhand Elite"] = {
		[1] = 4429,
	},
	["Captain Drenn"] = {
		[1] = 3378,
	},
	["Wandering Forest Walker"] = {
		[1] = 2627,
	},
	["Chromatic Dragonspawn"] = {
		[1] = 3116,
	},
	["Sandfury Speaker"] = {
		[1] = 4046,
	},
	["Gasher"] = {
		[1] = -727,
	},
	["Glasshide Gazer"] = {
		[1] = 2896,
	},
	["Galak Wrangler"] = {
		[1] = 1356,
	},
	["Jadespine Basilisk"] = {
		[1] = 1670,
	},
	["Grizzled Ironfur Bear"] = {
		[1] = -19,
	},
	["Dabyrie Militia"] = {
		[1] = 2177,
	},
	["Lady Anacondra"] = {
		[1] = 1314,
	},
	["Gurubashi Bat Rider"] = {
		[1] = 3766,
	},
	["Malor the Zealous"] = {
		[1] = -931,
	},
	["Frostsaber Pride Watcher"] = {
		[1] = 3871,
	},
	["Vilebranch Speaker"] = {
		[1] = -621,
	},
	["Doomforge Dragoon"] = {
		[1] = 3038,
	},
	["Anvilrage Footman"] = {
		[1] = 2881,
	},
	["Elder Plainstrider"] = {
		[1] = 614,
	},
	["Gorishi Stinger"] = {
		[1] = 3215,
	},
	["Smolderthorn Berserker"] = {
		[1] = 2402,
	},
	["Risen Guard"] = {
		[1] = 1092,
	},
	["Twilight Keeper Havunth"] = {
		[1] = 3727,
	},
	["Savannah Highmane"] = {
		[1] = 878,
	},
	["Vilebranch Witch Doctor"] = {
		[1] = 2080,
	},
	["Boulderfist Magus"] = {
		[1] = 3196,
	},
	["Ragnaros"] = {
		[1] = 4653,
	},
	["Young Mesa Buzzard"] = {
		[1] = 2000,
	},
	["Diseased Black Bear"] = {
		[1] = 3051,
	},
	["Blackhand Assassin"] = {
		[1] = 1599,
	},
	["Giant Plains Creeper"] = {
		[1] = 2374,
	},
	["Lava Reaver"] = {
		[1] = 5832,
	},
	["The Ravenian"] = {
		[1] = 329,
	},
	["Rage Talon Captain"] = {
		[1] = 4534,
	},
	["Plugger Spazzring"] = {
		[1] = 3469,
	},
	["Plagued Insect"] = {
		[1] = 3327,
	},
	["Highland Thrasher"] = {
		[1] = 2372,
	},
	["Zealot Lor'Khan"] = {
		[1] = 3243,
	},
	["Murk Spitter"] = {
		[1] = 2701,
	},
	["Savannah Cub"] = {
		[1] = 312,
	},
	["Wastewander Rogue"] = {
		[1] = 2824,
	},
	["Lady Illucia Barov"] = {
		[1] = -188,
	},
	["Smolderthorn Seer"] = {
		[1] = 3225,
	},
	["Spire Spiderling"] = {
		[1] = 2258,
	},
	["Ridge Stalker Patriarch"] = {
		[1] = 3029,
	},
	["Brokespear"] = {
		[1] = 1142,
	},
	["Carrion Swarmer"] = {
		[1] = 3173,
	},
	["Scarlet Soldier"] = {
		[1] = 2041,
	},
	["Moam"] = {
		[1] = -729,
	},
	["Warpwood Treant"] = {
		[1] = -1484,
	},
	["Southsea Brigand"] = {
		[1] = 918,
	},
	["Plated Stegodon"] = {
		[1] = 3414,
	},
	["Gurubashi Axe Thrower"] = {
		[1] = 2248,
	},
	["Razormane Water Seeker"] = {
		[1] = 612,
	},
	["Hazzas"] = {
		[1] = -1244,
	},
	["Bael'dun Officer"] = {
		[1] = 1820,
	},
	["Hive'Ashi Worker"] = {
		[1] = 3535,
	},
	["Vile Scarab"] = {
		[1] = 773,
	},
	["Highland Strider"] = {
		[1] = 2084,
	},
	["Kreenig Snarlsnout"] = {
		[1] = 808,
	},
	["Razzashi Raptor"] = {
		[1] = 3549,
	},
	["Un'Goro Gorilla"] = {
		[1] = 3525,
	},
	["Mother Smolderweb"] = {
		[1] = 1027,
	},
	["Enthralled Atal'ai"] = {
		[1] = 2786,
	},
	["Scarlet Guardsman"] = {
		[1] = 2983,
	},
	["Galak Stormer"] = {
		[1] = 1684,
	},
	["Thunderhawk Cloudscraper"] = {
		[1] = 1421,
	},
	["Woodpaw Trapper"] = {
		[1] = 2482,
	},
	["Pesterhide Hyena"] = {
		[1] = 2098,
	},
	["Atal'ai Priest"] = {
		[1] = -919,
	},
	["Gordunni Ogre Mage"] = {
		[1] = 2623,
	},
	["Scarlet Defender"] = {
		[1] = 3194,
	},
	["Sunscale Lashtail"] = {
		[1] = 696,
	},
	["Ornery Plainstrider"] = {
		[1] = 1240,
	},
	["Phalanx"] = {
		[1] = 3262,
	},
	["Hive'Zara Stinger"] = {
		[1] = 4531,
	},
	["Bloodaxe Warmonger"] = {
		[1] = 357,
	},
	["Spectral Assassin"] = {
		[1] = -2135,
	},
	["Magma Elemental"] = {
		[1] = 2955,
	},
	["Scarlet Myrmidon"] = {
		[1] = 2597,
	},
	["Twilight Master"] = {
		[1] = 4508,
	},
	["Spectral Teacher"] = {
		[1] = -544,
	},
	["Plagued Rat"] = {
		[1] = 3317,
	},
	["Winterfall Den Watcher"] = {
		[1] = 3987,
	},
	["Hakkari Oracle"] = {
		[1] = 2994,
	},
	["Zolo"] = {
		[1] = -498,
	},
	["Dark Guard"] = {
		[1] = 2859,
	},
	["Wildspawn Shadowstalker"] = {
		[1] = -2027,
	},
	["Risen Warrior"] = {
		[1] = 86,
	},
	["Diemetradon"] = {
		[1] = 3369,
	},
	["Nightmare Wyrmkin"] = {
		[1] = -824,
	},
	["Blackhand Dreadweaver"] = {
		[1] = 2797,
	},
	["Firebrand Grunt"] = {
		[1] = 229,
	},
	["Scarlet Invoker"] = {
		[1] = 3193,
	},
	["Eye of Immol'thar"] = {
		[1] = 4472,
	},
	["Hecklefang Snarler"] = {
		[1] = 1385,
	},
	["Tar Beast"] = {
		[1] = 3350,
	},
	["Incendosaur"] = {
		[1] = 3064,
	},
	["Frenzied Pterrordax"] = {
		[1] = 4258,
	},
	["Deviate Shambler"] = {
		[1] = 1257,
	},
	["Deviate Creeper"] = {
		[1] = 1000,
	},
	["Anvilrage Soldier"] = {
		[1] = 1823,
	},
	["Windfury Sorceress"] = {
		[1] = 727,
	},
	["Galak Mauler"] = {
		[1] = 1674,
	},
	["Deep Stinger"] = {
		[1] = 3283,
	},
	["The Rake"] = {
		[1] = 1669,
	},
	["Southsea Swashbuckler"] = {
		[1] = 2627,
	},
	["Ossirian the Unscarred"] = {
		[1] = -513,
	},
	["Firegut Ogre Mage"] = {
		[1] = 3258,
	},
	["Guard Fengus"] = {
		[1] = 3336,
	},
	["Dark Shade"] = {
		[1] = -419,
	},
	["Tyrant Devilsaur"] = {
		[1] = -1699,
	},
	["Lava Annihilator"] = {
		[1] = -57,
	},
	["Vilebranch Kidnapper"] = {
		[1] = 2756,
	},
	["Redstone Basilisk"] = {
		[1] = 2933,
	},
	["Withered Mistress"] = {
		[1] = 1982,
	},
	["Flamewaker Priest"] = {
		[1] = -1092,
	},
	["Ironbark Protector"] = {
		[1] = 6509,
	},
	["Hoary Templar"] = {
		[1] = 3582,
	},
	["Greater Duskbat"] = {
		[1] = 458,
	},
	["Highperch Wyvern"] = {
		[1] = 1885,
	},
	["Glutinous Ooze"] = {
		[1] = 3518,
	},
	["Needles Cougar"] = {
		[1] = 1794,
	},
	["Wastewander Shadow Mage"] = {
		[1] = 2444,
	},
	["Southsea Cannoneer"] = {
		[1] = 960,
	},
	["Thuzadin Necromancer"] = {
		[1] = -1950,
	},
	["High Priest Thekal"] = {
		[1] = 4559,
	},
	["Boss Tho'grun"] = {
		[1] = 2577,
	},
	["Dreamscythe"] = {
		[1] = 2275,
	},
	["Razzashi Serpent"] = {
		[1] = 3684,
	},
	["Chromatic Whelp"] = {
		[1] = 2385,
	},
	["Emperor Dagran Thaurissan"] = {
		[1] = 3200,
	},
	["Overmaster Pyron"] = {
		[1] = 2667,
	},
	["Guzzling Patron"] = {
		[1] = 1356,
	},
	["Scarshield Grunt"] = {
		[1] = -1386,
	},
	["Cursed Darkhound"] = {
		[1] = 628,
	},
	["Razzashi Broodwidow"] = {
		[1] = 3840,
	},
	["Barrens Giraffe"] = {
		[1] = 1092,
	},
	["Theldren"] = {
		[1] = -2706,
	},
	["Thuzadin Acolyte"] = {
		[1] = 1,
	},
	["Durotar Tiger"] = {
		[1] = 554,
	},
	["Zul'Lor"] = {
		[1] = -84,
	},
	["Reanimated Corpse"] = {
		[1] = -550,
	},
	["Lord Alexei Barov"] = {
		[1] = -16,
	},
	["Skeletal Highborne"] = {
		[1] = 3222,
	},
	["Crypt Crawler"] = {
		[1] = 586,
	},
	["Witherbark Troll"] = {
		[1] = 2164,
	},
	["Scarlet Tracking Hound"] = {
		[1] = 2626,
	},
	["Gurubashi Champion"] = {
		[1] = 3879,
	},
	["Goraluk Anvilcrack"] = {
		[1] = -422,
	},
	["Ridge Huntress"] = {
		[1] = 2483,
	},
	["Oasis Snapjaw"] = {
		[1] = 1009,
	},
	["Affray Challenger"] = {
		[1] = 1810,
	},
	["Sulfuron Harbinger"] = {
		[1] = -991,
	},
	["Flame Imp"] = {
		[1] = 3135,
	},
	["Sandfury Executioner"] = {
		[1] = 2496,
	},
	["Ashmane Boar"] = {
		[1] = 3306,
	},
	["Jed Runewatcher"] = {
		[1] = -1464,
	},
	["Gordunni Brute"] = {
		[1] = 2697,
	},
	["Shardtooth Mauler"] = {
		[1] = 3617,
	},
	["Muculent Ooze"] = {
		[1] = 3160,
	},
	["Rotting Highborne"] = {
		[1] = 3230,
	},
	["Dark Iron Lookout"] = {
		[1] = 2995,
	},
	["Spitelash Warrior"] = {
		[1] = 2830,
	},
	["Stormpike Defender"] = {
		[1] = 4117,
	},
	["Ravaged Cadaver"] = {
		[1] = -2576,
	},
	["Azure Templar"] = {
		[1] = 3581,
	},
	["Petrified Treant"] = {
		[1] = 14343,
	},
	["Highperch Consort"] = {
		[1] = 1862,
	},
	["Scarlet Gallant"] = {
		[1] = 2184,
	},
	["Blisterpaw Hyena"] = {
		[1] = 2807,
	},
	["Nerub'enkan"] = {
		[1] = 5476,
	},
	["Bloodpetal Pest"] = {
		[1] = 2129,
	},
	["Centipaar Tunneler"] = {
		[1] = 3073,
	},
	["Baroness Anastari"] = {
		[1] = -653,
	},
	["Black Guard Swordsmith"] = {
		[1] = 27,
	},
	["Anvilrage Marshal"] = {
		[1] = 2556,
	},
	["Gravelsnout Kobold"] = {
		[1] = 1717,
	},
	["Helboar"] = {
		[1] = 3218,
	},
	["Stone Guardian"] = {
		[1] = 3967,
	},
	["Bhag'thera"] = {
		[1] = 2503,
	},
	["Jin'do the Hexxer"] = {
		[1] = 2704,
	},
	["Fleshflayer Ghoul"] = {
		[1] = 3566,
	},
	["Hive'Ashi Sandstalker"] = {
		[1] = 3616,
	},
	["Lord Pythas"] = {
		[1] = 1316,
	},
	["Sunscale Screecher"] = {
		[1] = 965,
	},
	["Tendris Warpwood"] = {
		[1] = 3697,
	},
	["Gorishi Tunneler"] = {
		[1] = 3300,
	},
	["Spectral Citizen"] = {
		[1] = -424,
	},
	["Crimson Guardsman"] = {
		[1] = 483,
	},
	["Gordunni Ogre"] = {
		[1] = 2608,
	},
	["Galak Marauder"] = {
		[1] = 1709,
	},
	["Southsea Dock Worker"] = {
		[1] = 2748,
	},
	["Ramstein the Gorger"] = {
		[1] = 3654,
	},
	["Mana Burst"] = {
		[1] = 3003,
	},
	["Fireguard"] = {
		[1] = 2386,
	},
	["Black Guard Sentry"] = {
		[1] = 47,
	},
	["Dark Iron Taskmaster"] = {
		[1] = 1583,
	},
	["Razzashi Skitterer"] = {
		[1] = 3006,
	},
	["Weapon Technician"] = {
		[1] = 76,
	},
	["Earthen Templar"] = {
		[1] = 3563,
	},
	["Scarshield Sentry"] = {
		[1] = -245,
	},
	["Plague Ghoul"] = {
		[1] = 3224,
	},
	["Ice Thistle Matriarch"] = {
		[1] = 3754,
	},
	["Scarlet Scout"] = {
		[1] = 1902,
	},
	["Onyxian Whelp"] = {
		[1] = 3117,
	},
	["Wildspawn Felsworn"] = {
		[1] = -2399,
	},
	["Magmadar"] = {
		[1] = 3864,
	},
	["Quartermaster Zigris"] = {
		[1] = 2938,
	},
	["Spirestone Ogre Magus"] = {
		[1] = 231,
	},
	["Galak Scout"] = {
		[1] = 1730,
	},
	["The Unforgiven"] = {
		[1] = 1417,
	},
	["Taloned Swoop"] = {
		[1] = 546,
	},
	["Shadowsworn Thug"] = {
		[1] = 3281,
	},
	["Blazing Elemental"] = {
		[1] = 3233,
	},
	["Ogom the Wretched"] = {
		[1] = -981,
	},
	["Boulderfist Ogre"] = {
		[1] = 2545,
	},
	["Lord Incendius"] = {
		[1] = 2981,
	},
	["High Priestess Jeklik"] = {
		[1] = 4056,
	},
	["Sul'lithuz Sandcrawler"] = {
		[1] = 2911,
	},
	["Eldreth Spectre"] = {
		[1] = 2428,
	},
	["Boulderfist Brute"] = {
		[1] = 2416,
	},
	["Diseased Ghoul"] = {
		[1] = -460,
	},
	["Thaurissan Agent"] = {
		[1] = 3509,
	},
	["Razormane Mystic"] = {
		[1] = 641,
	},
	["Twilight Bodyguard"] = {
		[1] = 2321,
	},
	["Thaurissan Firewalker"] = {
		[1] = 3462,
	},
	["Stromgarde Troll Hunter"] = {
		[1] = 2888,
	},
	["Imp Minion"] = {
		[1] = 1555,
	},
	["Vile Priestess Hexx"] = {
		[1] = 3076,
	},
	["Southsea Freebooter"] = {
		[1] = 2557,
	},
	["Devouring Ectoplasm"] = {
		[1] = 1074,
	},
	["Nightmare Wanderer"] = {
		[1] = 2750,
	},
	["Spawn of Mar'li"] = {
		[1] = 3602,
	},
	["Gordok Captain"] = {
		[1] = 2863,
	},
	["Sandfury Slave"] = {
		[1] = 2707,
	},
	["Razormane Defender"] = {
		[1] = 703,
	},
	["Theramore Marine"] = {
		[1] = 1625,
	},
	["Spirestone Mystic"] = {
		[1] = -433,
	},
	["Drywallow Vicejaw"] = {
		[1] = 2351,
	},
	["Majordomo Executus"] = {
		[1] = -308,
	},
	["Blazing Fireguard"] = {
		[1] = 2351,
	},
	["High Priestess Mar'li"] = {
		[1] = 3971,
	},
	["Firebrand Invoker"] = {
		[1] = -167,
	},
	["Gehennas"] = {
		[1] = -213,
	},
	["Zulian Panther"] = {
		[1] = 3551,
	},
	["Unstable Corpse"] = {
		[1] = -2032,
	},
	["Zulian Tiger"] = {
		[1] = 3445,
	},
	["Lava Spawn"] = {
		[1] = 1194,
	},
	["Vilebranch Soul Eater"] = {
		[1] = 3037,
	},
	["Nightmare Scalebane"] = {
		[1] = 864,
	},
	["Cannon Master Willey"] = {
		[1] = 520,
	},
	["Dark Iron Steamsmith"] = {
		[1] = 2959,
	},
	["Ghoul Ravener"] = {
		[1] = 3281,
	},
	["Gorishi Reaver"] = {
		[1] = 2999,
	},
	["Risen Aberration"] = {
		[1] = -1400,
	},
	["Doomguard Minion"] = {
		[1] = 20,
	},
	["Flamewaker"] = {
		[1] = 4412,
	},
	["Tethis"] = {
		[1] = 2575,
	},
	["Hive'Zara Hornet"] = {
		[1] = 3662,
	},
	["Vile'rel"] = {
		[1] = 2571,
	},
	["Twilight Stonecaller"] = {
		[1] = 3444,
	},
	["Rockwing Gargoyle"] = {
		[1] = 3996,
	},
	["Elder Mottled Boar"] = {
		[1] = 535,
	},
	["Shadowforge Darkweaver"] = {
		[1] = 2739,
	},
	["Gordok Ogre-Mage"] = {
		[1] = 3414,
	},
	["Scarshield Worg"] = {
		[1] = 836,
	},
	["Atal'ai Deathwalker"] = {
		[1] = 1422,
	},
	["Corrupted Bloodtalon Scythemaw"] = {
		[1] = 890,
	},
	["Vilebranch Warrior"] = {
		[1] = 2317,
	},
	["Hakkari Shadowcaster"] = {
		[1] = 2774,
	},
	["Major Yeggeth"] = {
		[1] = 1140,
	},
	["Atal'ai Corpse Eater"] = {
		[1] = 1657,
	},
	["Lava Surger"] = {
		[1] = -25,
	},
	["Flamewaker Healer"] = {
		[1] = 3775,
	},
	["Illyanna Ravenoak"] = {
		[1] = -601,
	},
	["Searing Lava Spider"] = {
		[1] = 3022,
	},
	["Prince Tortheldrin"] = {
		[1] = -2079,
	},
	["Hive'Ashi Drone"] = {
		[1] = 3826,
	},
	["Burrowing Thundersnout"] = {
		[1] = 2628,
	},
	["Hakkari Blood Priest"] = {
		[1] = 2987,
	},
	["Jade Sludge"] = {
		[1] = 2911,
	},
	["Gravelsnout Vermin"] = {
		[1] = 1947,
	},
	["Onyxia"] = {
		[1] = -683,
	},
	["Dustbelcher Mauler"] = {
		[1] = 2838,
	},
	["Tar Lurker"] = {
		[1] = 3457,
	},
	["Mijan"] = {
		[1] = -676,
	},
	["Colonel Zerran"] = {
		[1] = -486,
	},
	["Hukku"] = {
		[1] = 441,
	},
	["Gordok Mauler"] = {
		[1] = 3036,
	},
	["Witherbark Speaker"] = {
		[1] = 2113,
	},
	["Rookery Whelp"] = {
		[1] = -142,
	},
	["Burning Exile"] = {
		[1] = 2695,
	},
	["Tar Creeper"] = {
		[1] = 3337,
	},
	["Grimtotem Bandit"] = {
		[1] = 1643,
	},
	["Razormane Wolf"] = {
		[1] = 773,
	},
	["Immol'thar"] = {
		[1] = 3980,
	},
	["Mad Servant"] = {
		[1] = -130,
	},
	["Muck Frenzy"] = {
		[1] = 1083,
	},
	["Splintered Skeleton"] = {
		[1] = 553,
	},
	["Ragefire Shaman"] = {
		[1] = 799,
	},
	["Kolkar Wrangler"] = {
		[1] = 881,
	},
	["Hive'Ashi Stinger"] = {
		[1] = 3385,
	},
	["Warpwood Tangler"] = {
		[1] = -1915,
	},
	["Razorbeak Skylord"] = {
		[1] = 2959,
	},
	["Blackhand Veteran"] = {
		[1] = 3499,
	},
	["Zulian Cub"] = {
		[1] = -1193,
	},
	["Shadowforge Senator"] = {
		[1] = 2063,
	},
	["Diseased Wolf"] = {
		[1] = 3149,
	},
	["Eldreth Apparition"] = {
		[1] = 2702,
	},
	["Wildspawn Betrayer"] = {
		[1] = -1773,
	},
	["Scholomance Dark Summoner"] = {
		[1] = 2791,
	},
	["Galak Assassin"] = {
		[1] = 1914,
	},
	["Crimson Monk"] = {
		[1] = -1026,
	},
	["Instructor Malicia"] = {
		[1] = 2534,
	},
	["Galak Messenger"] = {
		[1] = 1765,
	},
	["Stonevault Rockchewer"] = {
		[1] = -2671,
	},
	["Caliph Scorpidsting"] = {
		[1] = 2320,
	},
	["Steeljaw Snapper"] = {
		[1] = 2778,
	},
	["Anubisath Guardian"] = {
		[1] = -495,
	},
	["Warpwood Guardian"] = {
		[1] = -1635,
	},
	["Silithid Swarmer"] = {
		[1] = 1649,
	},
	["Mindless Undead"] = {
		[1] = 3369,
	},
	["Bloodtalon Scythemaw"] = {
		[1] = 572,
	},
	["Vampiric Duskbat"] = {
		[1] = 570,
	},
	["Razzashi Venombrood"] = {
		[1] = 3762,
	},
	["Spirestone Battle Mage"] = {
		[1] = 2529,
	},
	["War Reaver"] = {
		[1] = 3542,
	},
	["Molten War Golem"] = {
		[1] = 3189,
	},
	["Sunscale Scytheclaw"] = {
		[1] = 1219,
	},
	["Cursed Atal'ai"] = {
		[1] = 2310,
	},
	["Dustbelcher Brute"] = {
		[1] = 2526,
	},
	["Magmus"] = {
		[1] = 3261,
	},
	["Magistrate Barthilas"] = {
		[1] = 390,
	},
	["Rogue Vale Screecher"] = {
		[1] = -57,
	},
	["Syndicate Mercenary"] = {
		[1] = 2133,
	},
	["Dope'rel"] = {
		[1] = 1582,
	},
	["Greater Rock Elemental"] = {
		[1] = 2669,
	},
	["Pyromancer Loregrain"] = {
		[1] = -2420,
	},
	["Cho'Rush the Observer"] = {
		[1] = 2914,
	},
	["Stormsnout"] = {
		[1] = 1298,
	},
	["Rak'shiri"] = {
		[1] = 3738,
	},
	["Blackhand Incarcerator"] = {
		[1] = 3377,
	},
	["Razorfen Servitor"] = {
		[1] = 1630,
	},
	["Ridge Stalker"] = {
		[1] = 2399,
	},
	["Cave Creeper"] = {
		[1] = 3391,
	},
	["Prairie Wolf Alpha"] = {
		[1] = 614,
	},
	["Arcane Torrent"] = {
		[1] = 3595,
	},
	["Venomhide Ravasaur"] = {
		[1] = 3312,
	},
	["Plagued Maggot"] = {
		[1] = 3542,
	},
	["Wandering Barrens Giraffe"] = {
		[1] = 1157,
	},
	["Arcane Feedback"] = {
		[1] = 3343,
	},
	["Savannah Huntress"] = {
		[1] = 869,
	},
	["Marduk Blackpool"] = {
		[1] = 1522,
	},
	["Arcanist Doan"] = {
		[1] = 2171,
	},
	["Ice Thistle Yeti"] = {
		[1] = 3371,
	},
	["Shadowsworn Cultist"] = {
		[1] = 3090,
	},
	["Vilebranch Raiding Wolf"] = {
		[1] = 3086,
	},
	["Lord Roccor"] = {
		[1] = -566,
	},
	["Ribbly Screwspigot"] = {
		[1] = -780,
	},
	["Warchief Rend Blackhand"] = {
		[1] = 3317,
	},
	["Doom'rel"] = {
		[1] = 3082,
	},
	["Flamewaker Protector"] = {
		[1] = -702,
	},
	["Sandfury Soul Eater"] = {
		[1] = 2882,
	},
	["Bael'dun Rifleman"] = {
		[1] = 1445,
	},
	["Scarlet Chaplain"] = {
		[1] = 3173,
	},
	["Qiraji Warrior"] = {
		[1] = 4460,
	},
	["Core Rager"] = {
		[1] = 4746,
	},
	["Surf Glider"] = {
		[1] = 3140,
	},
	["Mosh'Ogg Witch Doctor"] = {
		[1] = 2639,
	},
	["Gyth"] = {
		[1] = 4123,
	},
	["Bloodhound Mastiff"] = {
		[1] = 2954,
	},
	["Zealot Zath"] = {
		[1] = 3742,
	},
	["Vilebranch Ambusher"] = {
		[1] = 2980,
	},
	["Mosh'Ogg Brute"] = {
		[1] = 1961,
	},
	["Bristleback Geomancer"] = {
		[1] = 1359,
	},
	["Hecklefang Hyena"] = {
		[1] = 1007,
	},
	["Overseer Maltorius"] = {
		[1] = 3238,
	},
	["Firewalker"] = {
		[1] = 2491,
	},
	["Flesh Hunter"] = {
		[1] = 3230,
	},
	["Blackhand Dragon Handler"] = {
		[1] = 5298,
	},
	["Woodpaw Brute"] = {
		[1] = 2556,
	},
	["Crag Coyote"] = {
		[1] = 2294,
	},
	["Anvilrage Officer"] = {
		[1] = 1235,
	},
	["Hakkari Witch Doctor"] = {
		[1] = 2896,
	},
	["Mummified Atal'ai"] = {
		[1] = -1817,
	},
	["Thundering Exile"] = {
		[1] = 2663,
	},
	["Swarmguard Needler"] = {
		[1] = 1490,
	},
	["Ras Frostwhisper"] = {
		[1] = -744,
	},
	["Shen'dralar Ancient"] = {
		[1] = -4,
	},
	["Summoned Water Elemental"] = {
		[1] = 38,
	},
	["Balnazzar"] = {
		[1] = 807,
	},
	["Ghostly Citizen"] = {
		[1] = -43,
	},
	["Plains Creeper"] = {
		[1] = 2264,
	},
	["Cadaverous Worm"] = {
		[1] = 4131,
	},
	["Magister Kalendris"] = {
		[1] = 2980,
	},
	["Tar Lord"] = {
		[1] = 4170,
	},
	["War Master Voone"] = {
		[1] = -202,
	},
	["Elder Crag Coyote"] = {
		[1] = 2570,
	},
	["Skeletal Berserker"] = {
		[1] = 2576,
	},
	["Vilebranch Blood Drinker"] = {
		[1] = 2750,
	},
	["Elder Cloud Serpent"] = {
		[1] = 1676,
	},
	["Scholomance Adept"] = {
		[1] = -570,
	},
	["Spitelash Screamer"] = {
		[1] = 2812,
	},
	["Crypt Beast"] = {
		[1] = 815,
	},
	["Vilebranch Shadowcaster"] = {
		[1] = 2911,
	},
	["Gizrul the Slavener"] = {
		[1] = -315,
	},
	["The Beast"] = {
		[1] = -295,
	},
	["Dark Screecher"] = {
		[1] = 2885,
	},
	["Hakkari Priest"] = {
		[1] = 2934,
	},
	["Verek"] = {
		[1] = 2997,
	},
	["Atal'ai High Priest"] = {
		[1] = 1455,
	},
	["Murkgill Warrior"] = {
		[1] = 2275,
	},
	["Bloodaxe Evoker"] = {
		[1] = -659,
	},
	["Watchman Doomgrip"] = {
		[1] = 3308,
	},
	["Gorosh the Dervish"] = {
		[1] = -1801,
	},
	["Monstrous Plaguebat"] = {
		[1] = 3589,
	},
	["Eldreth Spirit"] = {
		[1] = 3245,
	},
	["Gloom'rel"] = {
		[1] = 3120,
	},
	["Thunder Lizard"] = {
		[1] = 653,
	},
	["Deviate Stinglash"] = {
		[1] = 1269,
	},
	["Scarshield Warlock"] = {
		[1] = -205,
	},
	["Scarlet Beastmaster"] = {
		[1] = 2204,
	},
	["Glassweb Spider"] = {
		[1] = 2345,
	},
	["Greater Obsidian Elemental"] = {
		[1] = 3660,
	},
	["Ambassador Flamelash"] = {
		[1] = 2482,
	},
	["Obsidian Destroyer"] = {
		[1] = 4659,
	},
	["Burning Imp"] = {
		[1] = 2667,
	},
	["Bloodseeker Bat"] = {
		[1] = 3816,
	},
	["Grimtotem Stomper"] = {
		[1] = 1779,
	},
	["Syndicate Pathstalker"] = {
		[1] = 2172,
	},
	["Deviate Venomwing"] = {
		[1] = 1339,
	},
	["Ancient Core Hound"] = {
		[1] = 1549,
	},
	["Patchwork Horror"] = {
		[1] = 3644,
	},
	["Firebrand Darkweaver"] = {
		[1] = -11,
	},
	["Razzashi Adder"] = {
		[1] = 3642,
	},
	["Dustbelcher Wyrmhunter"] = {
		[1] = 2326,
	},
	["Risen Bonewarder"] = {
		[1] = 2027,
	},
	["General Angerforge"] = {
		[1] = 3205,
	},
	["Vilebranch Hideskinner"] = {
		[1] = 2955,
	},
	["Anvilrage Warden"] = {
		[1] = 2020,
	},
	["Overlord Wyrmthalak"] = {
		[1] = -1077,
	},
	["Treasure Hunting Swashbuckler"] = {
		[1] = 2867,
	},
	["Crimson Gallant"] = {
		[1] = 218,
	},
	["Bristleback Water Seeker"] = {
		[1] = 1099,
	},
	["Silithid Creeper"] = {
		[1] = 1214,
	},
	["Sandfury Blood Drinker"] = {
		[1] = 2964,
	},
	["Saltwater Snapjaw"] = {
		[1] = 3171,
	},
	["Dustbelcher Ogre Mage"] = {
		[1] = 3132,
	},
	["Seeth'rel"] = {
		[1] = 3864,
	},
	["Un'Goro Stomper"] = {
		[1] = 3338,
	},
	["Razormane Geomancer"] = {
		[1] = 949,
	},
	["Stonelash Scorpid"] = {
		[1] = 3241,
	},
	["Zhevra Runner"] = {
		[1] = 845,
	},
	["Mesa Buzzard"] = {
		[1] = 2157,
	},
	["Black Broodling"] = {
		[1] = 3318,
	},
	["Atal'ai Slave"] = {
		[1] = 2882,
	},
	["Scarshield Raider"] = {
		[1] = 643,
	},
	["Bloodaxe Raider"] = {
		[1] = -359,
	},
	["Kul Tiras Marine"] = {
		[1] = 434,
	},
	["Deviate Coiler"] = {
		[1] = 1000,
	},
	["Ravasaur Hunter"] = {
		[1] = -1930,
	},
	["Flameguard"] = {
		[1] = 670,
	},
	["Wastewander Scofflaw"] = {
		[1] = 2874,
	},
	["Blackhand Thug"] = {
		[1] = 2108,
	},
	["Crimson Priest"] = {
		[1] = -517,
	},
	["Fineous Darkvire"] = {
		[1] = 3504,
	},
	["Warpwood Crusher"] = {
		[1] = -2346,
	},
	["Heavy War Golem"] = {
		[1] = 3062,
	},
	["Anvilrage Medic"] = {
		[1] = 1484,
	},
	["Shrieking Banshee"] = {
		[1] = -1265,
	},
	["Anub'shiah"] = {
		[1] = 3518,
	},
	["Achellios the Banished"] = {
		[1] = 2127,
	},
	["Spirestone Reaver"] = {
		[1] = 486,
	},
	["Hecklefang Stalker"] = {
		[1] = 1296,
	},
	["Gordok Mastiff"] = {
		[1] = 3297,
	},
	["Blackhand Summoner"] = {
		[1] = 1957,
	},
	["Mana Fiend"] = {
		[1] = 532,
	},
	["Petrified Guardian"] = {
		[1] = 4842,
	},
	["Wildspawn Satyr"] = {
		[1] = 3632,
	},
	["Digger Flameforge"] = {
		[1] = 1586,
	},
	["Boulderfist Enforcer"] = {
		[1] = 2204,
	},
	["Pyroguard Emberseer"] = {
		[1] = 2291,
	},
	["Twilight Avenger"] = {
		[1] = 3481,
	},
	["Loro"] = {
		[1] = -63,
	},
	["Bael'dun Soldier"] = {
		[1] = 1848,
	},
	["Bloodaxe Summoner"] = {
		[1] = 2285,
	},
	["Spirestone Enforcer"] = {
		[1] = 538,
	},
	["Jungle Toad"] = {
		[1] = -2217,
	},
	["Crimson Initiate"] = {
		[1] = 1559,
	},
	["Silithid Harvester"] = {
		[1] = 1700,
	},
	["Dustbelcher Shaman"] = {
		[1] = 2703,
	},
	["Clattering Scorpid"] = {
		[1] = 453,
	},
	["Stonelash Pincer"] = {
		[1] = 3361,
	},
	["Lieutenant Benedict"] = {
		[1] = 1052,
	},
	["Stonelash Flayer"] = {
		[1] = 667,
	},
	["Skeletal Flayer"] = {
		[1] = 2982,
	},
	["High Priest Venoxis"] = {
		[1] = 4538,
	},
	["Deviate Crocolisk"] = {
		[1] = 1213,
	},
	["High Interrogator Gerstahn"] = {
		[1] = 2547,
	},
	["Scarlet Adept"] = {
		[1] = 2025,
	},
	["Shadow Hunter Vosh'gajin"] = {
		[1] = -37,
	},
	["Gordok Reaver"] = {
		[1] = 4597,
	},
	["Nethergarde Miner"] = {
		[1] = -1000,
	},
	["Bone Minion"] = {
		[1] = 78,
	},
	["Smolderthorn Shadow Priest"] = {
		[1] = 289,
	},
	["Scarlet Mage"] = {
		[1] = 3601,
	},
	["Gordok Brute"] = {
		[1] = 2618,
	},
	["Crimson Hammersmith"] = {
		[1] = -1919,
	},
	["Grimtotem Shaman"] = {
		[1] = 3262,
	},
	["Glasshide Basilisk"] = {
		[1] = 2720,
	},
	["Scarlet Hound"] = {
		[1] = 3073,
	},
	["Skeletal Terror"] = {
		[1] = 3127,
	},
	["Sand Skitterer"] = {
		[1] = 3292,
	},
	["Bloodaxe Veteran"] = {
		[1] = 2241,
	},
	["Dustwind Harpy"] = {
		[1] = 689,
	},
	["Scarlet Preserver"] = {
		[1] = 1879,
	},
	["Deviate Ravager"] = {
		[1] = 1089,
	},
	["Zulian Guardian"] = {
		[1] = 2953,
	},
	["Zulian Crocolisk"] = {
		[1] = 181,
	},
	["Sprite Darter"] = {
		[1] = -866,
	},
	["Ragefire Trogg"] = {
		[1] = 799,
	},
	["Theka the Martyr"] = {
		[1] = 2924,
	},
	["Deviate Slayer"] = {
		[1] = 1074,
	},
	["Baron Rivendare"] = {
		[1] = 2444,
	},
	["Venom Belcher"] = {
		[1] = 3841,
	},
	["Greater Lava Spider"] = {
		[1] = 3019,
	},
	["Deviate Lasher"] = {
		[1] = 1559,
	},
	["Elder Diemetradon"] = {
		[1] = 2914,
	},
	["Scarlet Spellbinder"] = {
		[1] = 3754,
	},
	["Swiftmane"] = {
		[1] = 1525,
	},
	["Cloned Ooze"] = {
		[1] = 3148,
	},
	["Princess Moira Bronzebeard"] = {
		[1] = 2716,
	},
	["Savannah Matriarch"] = {
		[1] = 1146,
	},
	["Soulflayer"] = {
		[1] = 3891,
	},
	["Feral Crag Coyote"] = {
		[1] = 2413,
	},
	["Pusillin"] = {
		[1] = -2920,
	},
	["Andre Firebeard"] = {
		[1] = 2745,
	},
	["Scarlet Avenger"] = {
		[1] = 3656,
	},
	["Gluggle"] = {
		[1] = 2255,
	},
	["Thaurissan Spy"] = {
		[1] = 3818,
	},
	["Skeletal Sorcerer"] = {
		[1] = 3050,
	},
	["Crag Stalker"] = {
		[1] = 1709,
	},
	["Savannah Prowler"] = {
		[1] = 956,
	},
	["Makasgar"] = {
		[1] = 1124,
	},
	["Crimson Templar"] = {
		[1] = 3581,
	},
	["Silvermane Stalker"] = {
		[1] = 4079,
	},
	["Venture Co. Geologist"] = {
		[1] = 2102,
	},
	["Searing Blade Warlock"] = {
		[1] = 575,
	},
	["Deviate Stalker"] = {
		[1] = 1203,
	},
	["Decrepit Darkhound"] = {
		[1] = 288,
	},
	["Jabbering Ghoul"] = {
		[1] = 3183,
	},
	["Dark Iron Sentry"] = {
		[1] = 3054,
	},
	["Captain Fairmount"] = {
		[1] = 1387,
	},
	["Dire Maul Crystal Totem"] = {
		[1] = 2089,
	},
	["Hurley Blackbreath"] = {
		[1] = 2913,
	},
	["Venomlash Scorpid"] = {
		[1] = 1581,
	},
	["Eldreth Darter"] = {
		[1] = 170,
	},
	["Greater Plainstrider"] = {
		[1] = 783,
	},
	["Venomous Cloud Serpent"] = {
		[1] = 1853,
	},
	["Scorpid Hunter"] = {
		[1] = 2834,
	},
	["Deviate Dreadfang"] = {
		[1] = 977,
	},
	["Rage Talon Fire Tongue"] = {
		[1] = 3790,
	},
	["Searing Blade Enforcer"] = {
		[1] = 826,
	},
	["Carrion Lurker"] = {
		[1] = 3174,
	},
	["Rage Talon Dragon Guard"] = {
		[1] = 5610,
	},
	["Spire Scorpid"] = {
		[1] = 3620,
	},
	["Vilebranch Berserker"] = {
		[1] = 2837,
	},
	["Witherbark Shadowcaster"] = {
		[1] = 2186,
	},
	["Baron Longshore"] = {
		[1] = 1363,
	},
	["Rock Elemental"] = {
		[1] = 2439,
	},
	["Wrath Phantom"] = {
		[1] = 2042,
	},
	["Crimson Conjuror"] = {
		[1] = -553,
	},
	["Highlord Omokk"] = {
		[1] = 651,
	},
	["Bloodpetal Trapper"] = {
		[1] = 3901,
	},
	["The Duke of Fathoms"] = {
		[1] = 3699,
	},
	["Crimson Battle Mage"] = {
		[1] = 971,
	},
	["Devilsaur"] = {
		[1] = 4972,
	},
	["Burning Blade Thug"] = {
		[1] = 848,
	},
	["Roc"] = {
		[1] = 2722,
	},
	["Stranglethorn Tigress"] = {
		[1] = 2174,
	},
	["Frostsaber Cub"] = {
		[1] = 3576,
	},
	["Bael'dun Foreman"] = {
		[1] = 1412,
	},
	["Sandfury Witch Doctor"] = {
		[1] = 2882,
	},
	["Smolderthorn Headhunter"] = {
		[1] = -878,
	},
	["Rock Stalker"] = {
		[1] = 3165,
	},
	["Atal'ai Mistress"] = {
		[1] = 3542,
	},
	["Vilebranch Shadow Hunter"] = {
		[1] = 3037,
	},
	["Kul Tiras Sailor"] = {
		[1] = 394,
	},
	["Dabyrie Laborer"] = {
		[1] = 2102,
	},
	["Maleki the Pallid"] = {
		[1] = 1938,
	},
	["Sandfury Shadowhunter"] = {
		[1] = 3228,
	},
	["Powerful Healing Ward"] = {
		[1] = 4303,
	},
	["Arnak Grimtotem"] = {
		[1] = 1626,
	},
	["Scarshield Legionnaire"] = {
		[1] = 2999,
	},
	["Hakkari Shadow Hunter"] = {
		[1] = 3156,
	},
	["Thauris Balgarr"] = {
		[1] = 3747,
	},
	["Burning Spirit"] = {
		[1] = 1850,
	},
	["Firesworn"] = {
		[1] = -262,
	},
	["Fire Elemental"] = {
		[1] = 81,
	},
	["Warbringer Construct"] = {
		[1] = -2,
	},
	["Tortured Druid"] = {
		[1] = 4138,
	},
	["Primal Ooze"] = {
		[1] = 2770,
	},
	["Wastewander Bandit"] = {
		[1] = 2719,
	},
	["Doomforge Craftsman"] = {
		[1] = 2900,
	},
	["Flatland Prowler"] = {
		[1] = 705,
	},
	["Fel Lash"] = {
		[1] = -2346,
	},
	["Makrura Clacker"] = {
		[1] = 738,
	},
	["Golem Lord Argelmach"] = {
		[1] = 2107,
	},
	["Burning Felguard"] = {
		[1] = 2750,
	},
	["Scholomance Occultist"] = {
		[1] = -468,
	},
	["Spectral Soldier"] = {
		[1] = 85,
	},
	["Dustbelcher Ogre"] = {
		[1] = 2639,
	},
	["Scalding Broodling"] = {
		[1] = 3501,
	},
	["Smolderthorn Witch Doctor"] = {
		[1] = 27,
	},
	["Bile Spewer"] = {
		[1] = 3544,
	},
	["Anvilrage Guardsman"] = {
		[1] = 3802,
	},
	["Smolderthorn Axe Thrower"] = {
		[1] = -20,
	},
	["Gordok Warlock"] = {
		[1] = 2376,
	},
	["Eldreth Phantasm"] = {
		[1] = 3300,
	},
	["Bloodpetal Thresher"] = {
		[1] = 3339,
	},
	["Frostsaber Huntress"] = {
		[1] = 3742,
	},
	["Dredge Striker"] = {
		[1] = 3292,
	},
	["Skeletal Shocktrooper"] = {
		[1] = 3512,
	},
	["Shadowforge Flame Keeper"] = {
		[1] = 3311,
	},
	["Fleeting Plainstrider"] = {
		[1] = 813,
	},
	["Elder Stranglethorn Tiger"] = {
		[1] = 1879,
	},
	["Grizzle"] = {
		[1] = 2750,
	},
	["Hammered Patron"] = {
		[1] = -22,
	},
	["Twilight Geolord"] = {
		[1] = 3501,
	},
	["Warpwood Stomper"] = {
		[1] = -1653,
	},
	["Zhevra Charger"] = {
		[1] = 1307,
	},
	["Borer Beetle"] = {
		[1] = -2310,
	},
	["Theramore Preserver"] = {
		[1] = 1219,
	},
	["Crimson Defender"] = {
		[1] = 1081,
	},
	["Shade of Eranikus"] = {
		[1] = 2654,
	},
	["Lesser Rock Elemental"] = {
		[1] = 2381,
	},
	["Starving Snickerfang"] = {
		[1] = 2064,
	},
	["Thunderhead"] = {
		[1] = 1358,
	},
	["Spirestone Butcher"] = {
		[1] = 2791,
	},
	["Core Hound"] = {
		[1] = 3165,
	},
	["Blighted Zombie"] = {
		[1] = 2742,
	},
	["Grand Crusader Dathrohan"] = {
		[1] = 3179,
	},
	["Kolkar Pack Runner"] = {
		[1] = 1007,
	},
	["Twilight Flamereaver"] = {
		[1] = 3725,
	},
	["Scarshield Spellbinder"] = {
		[1] = 2401,
	},
	["Bloodlord Mandokir"] = {
		[1] = 4247,
	},
	["Rage Talon Dragonspawn"] = {
		[1] = 3272,
	},
	["Twilight Dark Shaman"] = {
		[1] = 2766,
	},
	["Bloodpetal Lasher"] = {
		[1] = 3164,
	},
	["Anger'rel"] = {
		[1] = 3172,
	},
	["Rotting Cadaver"] = {
		[1] = 3152,
	},
	["Scarlet Diviner"] = {
		[1] = 2227,
	},
	["Firemane Scalebane"] = {
		[1] = 2412,
	},
	["General Rajaxx"] = {
		[1] = -332,
	},
	["Eldreth Sorcerer"] = {
		[1] = 1499,
	},
	["Woodpaw Mongrel"] = {
		[1] = 2416,
	},
	["Twilight Emissary"] = {
		[1] = 1919,
	},
	["Murderous Blisterpaw"] = {
		[1] = 2794,
	},
	["Qiraji Gladiator"] = {
		[1] = -301,
	},
	["Big Will"] = {
		[1] = 2247,
	},
	["Dreadmaw Crocolisk"] = {
		[1] = 478,
	},
	["Doctor Theolen Krastinov"] = {
		[1] = 961,
	},
	["Firegut Ogre"] = {
		[1] = 3248,
	},
	["Twilight's Hammer Torturer"] = {
		[1] = 2644,
	},
	["Ghoul Berserker"] = {
		[1] = 3512,
	},
	["Panzor the Invincible"] = {
		[1] = 3459,
	},
	["Blackhand Iron Guard"] = {
		[1] = 8260,
	},
	["Deviate Guardian"] = {
		[1] = 1130,
	},
	["Mercutio Filthgorger"] = {
		[1] = 3471,
	},
	["Lar'korwi Mate"] = {
		[1] = 3334,
	},
	["Harb Foulmountain"] = {
		[1] = 1847,
	},
	["Shadowpriest Sezz'ziz"] = {
		[1] = 2962,
	},
	["Kurinnaxx"] = {
		[1] = 4512,
	},
	["Voodoo Slave"] = {
		[1] = 1395,
	},
	["Lava Elemental"] = {
		[1] = 411,
	},
	["Giant Buzzard"] = {
		[1] = 2742,
	},
	["Twilight Overlord"] = {
		[1] = 3548,
	},
	["Treasure Hunting Pirate"] = {
		[1] = 2900,
	},
	["Crimson Inquisitor"] = {
		[1] = 462,
	},
	["Dire Mottled Boar"] = {
		[1] = 573,
	},
	["Slavering Ghoul"] = {
		[1] = 3017,
	},
	["Guard Mol'dar"] = {
		[1] = 3357,
	},
	["Nightmare Whelp"] = {
		[1] = -100,
	},
	["Wandering Eye of Kilrogg"] = {
		[1] = 3605,
	},
	["Dredge Crusher"] = {
		[1] = 3406,
	},
	["Tazan"] = {
		[1] = 810,
	},
	["Scarlet Sentry"] = {
		[1] = 1960,
	},
	["High Priestess Arlokk"] = {
		[1] = 3804,
	},
	["Evolving Ectoplasm"] = {
		[1] = 1074,
	},
	["Rockwing Screecher"] = {
		[1] = 638,
	},
	["Cliff Walker"] = {
		[1] = 3117,
	},
	["Thuzadin Shadowcaster"] = {
		[1] = -1534,
	},
	["Stormhide"] = {
		[1] = 1515,
	},
	["Lathoric the Black"] = {
		[1] = 3409,
	},
	["Fireguard Destroyer"] = {
		[1] = -12,
	},
	["Onyxian Warder"] = {
		[1] = 3310,
	},
	["Galak Windchaser"] = {
		[1] = 1601,
	},
	["Pesterhide Snarler"] = {
		[1] = 1958,
	},
	["Heggin Stonewhisker"] = {
		[1] = 1960,
	},
	["Prospector Khazgorm"] = {
		[1] = 1366,
	},
	["Bloodaxe Worg"] = {
		[1] = -471,
	},
	["Molten Giant"] = {
		[1] = -465,
	},
	["Bael'dun Excavator"] = {
		[1] = 1981,
	},
	["Frostsaber Stalker"] = {
		[1] = 3993,
	},
	["Ghost Howl"] = {
		[1] = 982,
	},
	["Earth Elemental"] = {
		[1] = 3590,
	},
	["Twilight's Hammer Ambassador"] = {
		[1] = 3682,
	},
	["Chromatic Elite Guard"] = {
		[1] = 2138,
	},
	["Gargantuan Ooze"] = {
		[1] = 3339,
	},
	["Ohgan"] = {
		[1] = 2653,
	},
	["Cloud Serpent"] = {
		[1] = 1489,
	},
	["Captain Kromcrush"] = {
		[1] = 3873,
	},
	["Halycon"] = {
		[1] = -145,
	},
	["Hive'Zara Wasp"] = {
		[1] = -430,
	},
	["Grand Inquisitor Isillien"] = {
		[1] = 3512,
	},
	["Darkmaster Gandling"] = {
		[1] = -155,
	},
	["Zulian Prowler"] = {
		[1] = 3384,
	},
	["Ferra"] = {
		[1] = 3712,
	},
	["Eldreth Seether"] = {
		[1] = 3329,
	},
	["Taragaman the Hungerer"] = {
		[1] = 40,
	},
	["Gurubashi Warrior"] = {
		[1] = 3233,
	},
	["Bristleback Hunter"] = {
		[1] = 1148,
	},
	["Gammerita"] = {
		[1] = 3092,
	},
	["Unliving Atal'ai"] = {
		[1] = -1886,
	},
	["Scarab"] = {
		[1] = 3025,
	},
	["Venomtail Scorpid"] = {
		[1] = 572,
	},
}
TheoryCraft_MitigationPlayers = {
	["Cheshirecat"] = {
		[1] = 2173,
	},
	["Rogue:60"] = {
		[1] = 1965,
	},
	["Hunter:42"] = {
		[1] = 674,
	},
	["Stayclassy"] = {
		[1] = 1265,
	},
	["Treasa"] = {
		[1] = 6591,
	},
	["Prillan"] = {
		[1] = 1779,
	},
	["Chocawar"] = {
		[1] = 3282,
	},
	["Waia"] = {
		[1] = 2721,
	},
	["Druid:60"] = {
		[1] = 1448,
	},
	["Warrior:60"] = {
		[1] = 2721,
	},
	["Dhube"] = {
		[1] = 3321,
	},
	["Job"] = {
		[1] = 6591,
	},
	["Vehement"] = {
		[1] = 8414,
	},
	["Hunter:60"] = {
		[1] = 2763,
	},
	["Mage:60"] = {
		[1] = 1076,
	},
	["Priest:60"] = {
		[1] = 2438,
	},
	["Oidde"] = {
		[1] = 2438,
	},
	["Lestaria"] = {
		[1] = 2763,
	},
	["Soyouthink"] = {
		[1] = 1965,
	},
	["Tropicana"] = {
		[1] = 1535,
	},
	["Fulgoa"] = {
		[1] = 674,
	},
	["Psolikarfi"] = {
		[1] = 1076,
	},
	["Celoa"] = {
		[1] = 2079,
	},
	["Peetr"] = {
		[1] = 1472,
	},
	["Sinit"] = {
		[1] = 781,
	},
	["Brucewilly"] = {
		[1] = 1088,
	},
	["Narrs"] = {
		[1] = 5961,
	},
	["Paladin:60"] = {
		[1] = 5961,
	},
	["Soulmonger"] = {
		[1] = 779,
	},
	["Warlock:60"] = {
		[1] = 1004,
	},
	["Fric"] = {
		[1] = 1004,
	},
	["Bulzen"] = {
		[1] = 1480,
	},
	["Derocian"] = {
		[1] = 1448,
	},
}
