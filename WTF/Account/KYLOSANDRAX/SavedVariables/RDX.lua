
RDX5Data = {
	["AutoPromoteList"] = {
	},
	["Global"] = {
		["DataVersion"] = 12,
		["fxLow"] = {
		},
		["perf"] = {
			["bNumUnitsPerAuraUpdate"] = 5,
			["bFollowDistanceUpdateDelay"] = 0.25,
			["mqMaxSize"] = 50,
			["bDeathUpdateDelay"] = 0.25,
			["hbFast"] = 0.2,
			["mqDelay"] = 0.75,
			["bAuraUpdateDelay"] = 0.25,
			["bAuraRealtimeSyncDelay"] = 2,
			["enableHOT"] = true,
			["uiWindowUpdateDelay"] = 0.25,
			["bAuraTimerSyncDelay"] = 10,
		},
		["vis"] = {
			["cStatusText"] = {
				["r"] = 1,
				["g"] = 1,
				["b"] = 1,
			},
			["cStatusTextFade"] = {
				["r"] = 1,
				["g"] = 0,
				["b"] = 0,
			},
			["cWindowTitle"] = {
				["r"] = 0.05,
				["g"] = 0.35,
				["b"] = 0.31,
			},
			["cManaFade"] = {
				["r"] = 1,
				["g"] = 0,
				["b"] = 0,
			},
			["cBuffWindowTitle"] = {
				["r"] = 0.1,
				["g"] = 0.7,
				["b"] = 0.1,
			},
			["cMana"] = {
				["r"] = 0,
				["g"] = 0,
				["b"] = 1,
			},
			["cEnemyHP"] = {
				["r"] = 0,
				["g"] = 1,
				["b"] = 0,
			},
			["cEnemyHPFade"] = {
				["r"] = 1,
				["g"] = 0,
				["b"] = 0,
			},
			["cDT"] = {
				["other"] = {
					["r"] = 0.5,
					["g"] = 0.5,
					["b"] = 0.5,
				},
				["disease"] = {
					["r"] = 0.5600000000000001,
					["g"] = 0.45,
					["b"] = 0.22,
				},
				["poison"] = {
					["r"] = 0.26,
					["g"] = 0.8,
					["b"] = 0.01,
				},
				["magic"] = {
					["r"] = 0.25,
					["g"] = 0.38,
					["b"] = 0.78,
				},
				["curse"] = {
					["r"] = 0.9,
					["g"] = 0.1,
					["b"] = 0,
				},
			},
			["cFriendHPFade"] = {
				["r"] = 1,
				["g"] = 0,
				["b"] = 0,
			},
			["cLinkdead"] = {
				["r"] = 0.5,
				["g"] = 0.5,
				["b"] = 0.5,
			},
			["cFriendHP"] = {
				["r"] = 0,
				["g"] = 1,
				["b"] = 0,
			},
			["cStaleData"] = {
				["r"] = 0.3,
				["g"] = 0.7,
				["b"] = 0.7,
			},
		},
		["mod_windows"] = {
			["raid healing"] = {
				["smbtn"] = {
					["iid"] = 1,
				},
				["scale"] = 1.216663479804993,
				["sort"] = 2,
				["srbtn"] = {
					["sptitle"] = "rejuvenation(rank 10)",
					["iid"] = 4,
				},
				["disp"] = 1,
				["stext"] = 3,
				["truncate"] = 8,
				["layout"] = 1,
				["width"] = 152.5184936523438,
				["rbtn"] = {
					["iid"] = 1,
				},
				["slbtn"] = {
					["sptitle"] = "healing touch(rank 4)",
					["iid"] = 4,
				},
				["name"] = "raid healing",
				["filter"] = {
					["desync"] = 3,
					["hpmin"] = 1,
					["hp"] = true,
					["followdistance"] = 2,
					["classes"] = {
						[1] = true,
						[2] = true,
						[3] = true,
						[4] = true,
						[5] = true,
						[7] = true,
						[8] = true,
						[9] = true,
					},
					["cg"] = true,
					["hpmax"] = 93,
					["mp"] = false,
					["hm"] = 1,
					["hptype"] = 1,
					["groups"] = {
						[1] = true,
						[2] = true,
						[3] = true,
						[4] = true,
						[5] = true,
						[6] = true,
						[7] = true,
						[8] = true,
					},
					["dead"] = 3,
				},
				["height"] = 24.99993515014648,
				["lbtn"] = {
					["iid"] = 2,
				},
				["mbtn"] = {
					["iid"] = 1,
				},
				["title"] = "Raid Healing",
			},
			["main tanks"] = {
				["smbtn"] = {
					["sptitle"] = "swiftmend",
					["iid"] = 4,
				},
				["scale"] = 1.216663479804993,
				["sort"] = 2,
				["srbtn"] = {
					["sptitle"] = "rejuvenation(rank 10)",
					["iid"] = 4,
				},
				["disp"] = 1,
				["stext"] = 3,
				["truncate"] = 5,
				["layout"] = 1,
				["width"] = 152.5184936523438,
				["name"] = "main tanks",
				["slbtn"] = {
					["sptitle"] = "healing touch(rank 4)",
					["iid"] = 4,
				},
				["title"] = "Main Tanks",
				["filter"] = {
					["desync"] = 3,
					["hpmin"] = 1,
					["hp"] = true,
					["dead"] = 3,
					["classes"] = {
						[1] = true,
					},
					["cg"] = true,
					["hpmax"] = 93,
					["mp"] = false,
					["hm"] = 1,
					["groups"] = {
						[1] = true,
						[2] = true,
					},
					["hptype"] = 1,
					["followdistance"] = 1,
				},
				["height"] = 24.99993515014648,
				["lbtn"] = {
					["iid"] = 2,
				},
				["mbtn"] = {
					["iid"] = 1,
				},
				["rbtn"] = {
					["iid"] = 1,
				},
			},
		},
	},
	["User"] = {
		["ryvok|warsong [12x] blizzlike"] = {
			["enabled"] = true,
			["venc"] = {
			},
			["enc_default"] = {
				["mod_debuffs"] = {
					["y"] = 1157.777913544159,
					["slice"] = 1,
					["relevance"] = {
					},
					["show"] = 4,
					["x"] = 311.1111449233229,
				},
				["mod_assists"] = {
					["auxarray"] = {
					},
					["mtarray"] = {
					},
				},
				["mod_windows"] = {
					["raid healing"] = {
						["y"] = 648.8889461626353,
						["shown"] = false,
						["x"] = 1611.111070915982,
					},
					["main tanks"] = {
						["y"] = 847.7779510653628,
						["shown"] = true,
						["x"] = 1607.778318514398,
					},
				},
			},
			["active_encounter"] = "default",
		},
		["ryjax|warsong [12x] blizzlike"] = {
			["enabled"] = true,
			["venc"] = {
			},
			["enc_default"] = {
				["mod_debuffs"] = {
					["show"] = 4,
					["x"] = 196.6665075250069,
					["relevance"] = {
					},
					["y"] = 1147.777801498079,
					["slice"] = 1,
					["filterConfig"] = {
						["desync"] = 3,
						["hp"] = false,
						["classes"] = {
							[1] = true,
							[2] = true,
							[3] = true,
							[4] = true,
							[5] = true,
							[6] = true,
							[7] = true,
							[8] = true,
							[9] = true,
						},
						["cg"] = true,
						["groups"] = {
							[1] = true,
							[2] = true,
							[3] = true,
							[4] = true,
							[5] = true,
							[6] = true,
							[7] = true,
							[8] = true,
						},
						["mp"] = false,
						["followdistance"] = 1,
						["dead"] = 3,
					},
				},
				["mod_assists"] = {
					["auxarray"] = {
					},
					["mtarray"] = {
					},
				},
				["mod_windows"] = {
					["main tanks"] = {
						["y"] = 543.1349359545388,
						["shown"] = false,
						["x"] = 1355.980825888285,
					},
					["raid healing"] = {
						["y"] = 625.9053422820111,
						["shown"] = false,
						["x"] = 1345.098206835768,
					},
				},
			},
			["noesync"] = true,
			["active_encounter"] = "default",
		},
		["kashonar|warsong [12x] blizzlike"] = {
			["enabled"] = true,
			["venc"] = {
			},
			["active_encounter"] = "default",
			["enc_default"] = {
				["mod_assists"] = {
					["auxarray"] = {
					},
					["mtarray"] = {
					},
				},
			},
		},
	},
	["BootstrapVer"] = 19,
	["ImpliedTarget"] = 0,
	["TrustedBootstrapSources"] = {
	},
	["Bootstrap"] = {
	},
	["RaidStatuscfg"] = {
		["y"] = 0,
		["active"] = false,
		["x"] = 0,
	},
}
