
ElkBuffBarOptions = {
	["alpha"] = 1,
	["scale"] = 1,
	["anchor"] = "TOPRIGHT",
	["dbcolor"] = true,
	["width"] = 250,
	["y"] = 1175,
	["x"] = 2844,
	["sort"] = "DEFAULT",
	["spacing"] = 0,
	["height"] = 20,
	["timer"] = "DEFAULT",
	["icon"] = "LEFT",
	["group"] = 5,
}
