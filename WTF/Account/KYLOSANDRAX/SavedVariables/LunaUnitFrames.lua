
LunaOptions = {
	["resIcon"] = "Interface\\AddOns\\LunaUnitFrames\\media\\Raid-Icon-Rez",
	["PowerColors"] = {
		["Rage"] = {
			[1] = 0.8862745098039215,
			[2] = 0.1764705882352941,
			[3] = 0.2941176470588235,
		},
		["Focus"] = {
			[1] = 1,
			[2] = 0.6980392156862745,
			[3] = 0,
		},
		["Mana"] = {
			[1] = 0.1882352941176471,
			[2] = 0.4431372549019608,
			[3] = 0.7490196078431373,
		},
		["Energy"] = {
			[1] = 1,
			[2] = 1,
			[3] = 0.1333333333333333,
		},
		["Happiness"] = {
			[1] = 0,
			[2] = 1,
			[3] = 1,
		},
	},
	["PartyRange"] = 1,
	["indicator"] = "Interface\\AddOns\\LunaUnitFrames\\media\\indicator",
	["frames"] = {
		["LunaPartyTargetFrames"] = {
			["enabled"] = 1,
			["position"] = {
				["y"] = 0,
				["x"] = 0,
			},
			["scale"] = 1,
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
			},
			["size"] = {
				["y"] = 20,
				["x"] = 110,
			},
		},
		["LunaTargetTargetFrame"] = {
			["enabled"] = 1,
			["scale"] = 1,
			["ShowBuffs"] = 3,
			["BuffInRow"] = 10,
			["position"] = {
				["y"] = -958.8898360620952,
				["x"] = 1821.111436553649,
			},
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
			},
			["size"] = {
				["y"] = 50,
				["x"] = 200,
			},
		},
		["LunaRaidFrames"] = {
			["pBars"] = 1,
			["scale"] = 1,
			["ShowRaidGroupTitles"] = 0,
			["width"] = 80,
			["height"] = 35,
			["padding"] = 4,
			["positions"] = {
				[1] = {
					["y"] = -400,
					["x"] = 400,
				},
				[2] = {
					["y"] = -400,
					["x"] = 400,
				},
				[3] = {
					["y"] = -400,
					["x"] = 400,
				},
				[4] = {
					["y"] = -400,
					["x"] = 400,
				},
				[5] = {
					["y"] = -400,
					["x"] = 400,
				},
				[6] = {
					["y"] = -400,
					["x"] = 400,
				},
				[7] = {
					["y"] = -400,
					["x"] = 400,
				},
				[8] = {
					["y"] = -400,
					["x"] = 400,
				},
			},
			["inverthealth"] = 1,
		},
		["LunaTargetTargetTargetFrame"] = {
			["enabled"] = 1,
			["scale"] = 1,
			["ShowBuffs"] = 3,
			["BuffInRow"] = 10,
			["position"] = {
				["y"] = -960.0002235746244,
				["x"] = 2053.333198774531,
			},
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
			},
			["size"] = {
				["y"] = 50,
				["x"] = 200,
			},
		},
		["LunaPlayerFrame"] = {
			["enabled"] = 1,
			["portrait"] = 2,
			["scale"] = 1,
			["ShowBuffs"] = 1,
			["position"] = {
				["y"] = -958.8896373290958,
				["x"] = 1051.111619501866,
			},
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
				[3] = {
					[1] = "Castbar",
					[2] = 3,
				},
				[4] = {
					[1] = "Druidbar",
					[2] = 0,
				},
				[5] = {
					[1] = "Totembar",
					[2] = 0,
				},
			},
			["size"] = {
				["y"] = 50,
				["x"] = 240,
			},
		},
		["LunaTargetFrame"] = {
			["enabled"] = 1,
			["portrait"] = 2,
			["scale"] = 1,
			["ShowBuffs"] = 3,
			["BuffInRow"] = 10,
			["position"] = {
				["y"] = -958.8890411300973,
				["x"] = 1551.111458548838,
			},
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
				[3] = {
					[1] = "Castbar",
					[2] = 3,
				},
				[4] = {
					[1] = "Combo Bar",
					[2] = 2,
				},
			},
			["size"] = {
				["y"] = 50,
				["x"] = 240,
			},
		},
		["LunaPartyFrames"] = {
			["enabled"] = 1,
			["portrait"] = 2,
			["scale"] = 1,
			["ShowBuffs"] = 4,
			["position"] = {
				["y"] = -371.1111216342995,
				["x"] = 492.2212638357824,
			},
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
			},
			["size"] = {
				["y"] = 40,
				["x"] = 200,
			},
		},
		["LunaPetFrame"] = {
			["enabled"] = 1,
			["portrait"] = 2,
			["scale"] = 1,
			["ShowBuffs"] = 3,
			["position"] = {
				["y"] = -70,
				["x"] = 10,
			},
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
			},
			["size"] = {
				["y"] = 30,
				["x"] = 240,
			},
		},
		["LunaPartyPetFrames"] = {
			["enabled"] = 1,
			["position"] = {
				["y"] = 0,
				["x"] = 0,
			},
			["scale"] = 1,
			["bars"] = {
				[1] = {
					[1] = "Healthbar",
					[2] = 6,
				},
				[2] = {
					[1] = "Powerbar",
					[2] = 4,
				},
			},
			["size"] = {
				["y"] = 20,
				["x"] = 110,
			},
		},
	},
	["XPBar"] = 1,
	["Rangefreq"] = 0.2,
	["statusbartexture"] = "Interface\\AddOns\\LunaUnitFrames\\media\\statusbar",
	["VerticalParty"] = 1,
	["enableRaid"] = 0,
	["DebuffTypeColor"] = {
		["Disease"] = {
			[1] = 0.6,
			[2] = 0.4,
			[3] = 0,
		},
		["Curse"] = {
			[1] = 0.6,
			[2] = 0,
			[3] = 1,
		},
		["Magic"] = {
			[1] = 0.2,
			[2] = 0.6,
			[3] = 1,
		},
		["Poison"] = {
			[1] = 0,
			[2] = 0.6,
			[3] = 0,
		},
	},
	["MiscColors"] = {
		["neutral"] = {
			[1] = 0.9300000000000001,
			[2] = 0.9300000000000001,
			["b"] = 0,
		},
		["static"] = {
			[1] = 0.7,
			[2] = 0.2,
			[3] = 0.9,
		},
		["friendly"] = {
			[1] = 0.2,
			[2] = 0.9,
			["b"] = 0.2,
		},
		["enemyUnattack"] = {
			[1] = 0.6,
			[2] = 0.2,
			[3] = 0.2,
		},
		["tapped"] = {
			[1] = 0.5,
			[2] = 0.5,
			[3] = 0.5,
		},
		["hostile"] = {
			[1] = 0.9,
			[2] = 0,
			[3] = 0,
		},
		["green"] = {
			[1] = 0.2,
			[2] = 0.9,
			[3] = 0.2,
		},
		["yellow"] = {
			[1] = 0.9300000000000001,
			[2] = 0.9300000000000001,
			[3] = 0,
		},
		["offline"] = {
			[1] = 0.5,
			[2] = 0.5,
			["b"] = 0.5,
		},
		["inc"] = {
			[1] = 0,
			[2] = 0.35,
			[3] = 0.23,
		},
		["red"] = {
			[1] = 0.9,
			[2] = 0,
			[3] = 0,
		},
	},
	["BTimers"] = 0,
	["overheal"] = 20,
	["hideCastbar"] = 1,
	["Raidbuff"] = "",
	["clickcast"] = {
		[1] = {
			[1] = "target",
			[2] = "menu",
		},
		[2] = {
			[1] = "Healing Touch(Rank 4)",
			[2] = "Rejuvenation(Rank 10)",
		},
		[3] = {
			[1] = "",
			[2] = "",
		},
		[4] = {
			[1] = "",
			[2] = "",
		},
	},
	["fontHeight"] = 11,
	["bordertexture"] = "Interface\\AddOns\\LunaUnitFrames\\media\\border",
	["backdrop"] = {
		["bgFile"] = "Interface\\Tooltips\\UI-Tooltip-Background",
		["tileSize"] = 16,
		["tile"] = true,
		["insets"] = {
			["top"] = -1.5,
			["bottom"] = -1.5,
			["right"] = -1.5,
			["left"] = -1.5,
		},
	},
	["icontexture"] = "Interface\\AddOns\\LunaUnitFrames\\media\\icon",
	["hideBlizzCastbar"] = 1,
	["font"] = "Interface\\AddOns\\LunaUnitFrames\\media\\barframes.ttf",
	["RaidRange"] = 1,
	["ClassColors"] = {
		["HUNTER"] = {
			[1] = 0.67,
			[2] = 0.83,
			[3] = 0.45,
		},
		["WARRIOR"] = {
			[1] = 0.78,
			[2] = 0.61,
			[3] = 0.43,
		},
		["ROGUE"] = {
			[1] = 1,
			[2] = 0.96,
			[3] = 0.41,
		},
		["MAGE"] = {
			[1] = 0.41,
			[2] = 0.8,
			[3] = 0.94,
		},
		["PRIEST"] = {
			[1] = 1,
			[2] = 1,
			[3] = 1,
		},
		["WARLOCK"] = {
			[1] = 0.58,
			[2] = 0.51,
			[3] = 0.79,
		},
		["DRUID"] = {
			[1] = 1,
			[2] = 0.49,
			[3] = 0.04,
		},
		["PALADIN"] = {
			[1] = 0.96,
			[2] = 0.55,
			[3] = 0.73,
		},
		["SHAMAN"] = {
			[1] = 0.14,
			[2] = 0.35,
			[3] = 1,
		},
	},
	["PartyinRaids"] = 0,
	["Raidlayout"] = "GRID",
	["PartySpace"] = 15,
	["EnergyTicker"] = 1,
}
